<?php
/**
 * Usuario Model
 *
 */
class Pagina extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'titulo';

	public $actsAs = ['Media.Media'];

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = [
		'titulo' => [
			'notBlank' => [
				'rule'    => 'notBlank',
				'message' => 'O título não pode ser vazio.'
			]
		]
	];
}
