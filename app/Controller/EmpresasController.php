<?php
App::uses('AppController', 'Controller');

class EmpresasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = ['Paginator', 'Flash', 'Session'];

	public function beforeFilter() {
		parent::beforeFilter();
		if ($this->isCMS()) {
			$this->set('module_config', [
				'title'       => 'Empresas',
				'description' => ''
			]);
		}
	}

/**
 * cms_index method
 *
 * @return void
 */
	public function cms_index() {
		$conditions = [];
		if (isset($this->request->query['busca']) && !empty($this->request->query['busca'])) {
			$conditions += ['Empresa.titulo LIKE' => '%'.$this->request->query['busca'].'%'];
			$this->request->data['Empresa'] = ['busca' => $this->request->query['busca']];
		}
		$this->Paginator->settings['conditions'] = $conditions;
		$this->Paginator->settings['order'] = ['Empresa.id' => 'desc'];
		$this->Empresa->recursive = 0;
		$this->set('the_list', $this->Paginator->paginate());
	}

/**
 * cms_add method
 *
 * @return void
 */
	public function cms_add() {
		$this->Empresa->create();
		if ($this->request->is(['post', 'put'])) {
			if ($this->Empresa->save($this->request->data))  {
				$this->Session->setFlash(__('Registro salvo com sucesso.'), 'cms_msg', ['class' => 'success'], 'cms');
				return $this->redirect(['cms' => true, 'action' => 'edit', $this->Empresa->id]);
			} else {
				$this->Session->setFlash(__('Ocorreu algum erro.'), 'cms_msg', ['class' => 'danger'], 'cms');
			}
		}
	}

/**
 * cms_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cms_edit($id = null) {
		if (!$this->Empresa->exists($id)) {
			throw new NotFoundException(__('Requisição inválida.'));
		}
		if ($this->request->is(['post', 'put'])) {
			if ($this->Empresa->save($this->request->data)) {
				$this->Session->setFlash(__('Registro atualizado com sucesso.'), 'cms_msg', ['class' => 'success'], 'cms');
				return $this->redirect(['action' => 'edit', 'cms' => true, $id]);
			} else {
				$this->Session->setFlash(__('Ocorreu algum erro.'), 'cms_msg', ['class' => 'danger'], 'cms');
			}
		} else {
			$this->request->data = $this->Empresa->read(null, $id);
		}
	}

/**
 * cms_remove method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cms_remove($id = null) {
		$this->Empresa->id = $id;
		if (!$this->Empresa->exists()) {
			throw new NotFoundException(__('Registro não encontrado.'));
		}
		if ($this->Empresa->delete()) {
			$this->Session->setFlash(__('Registro removido com sucesso.'), 'cms_msg', ['class' => 'success'], 'cms');
		} else {
			$this->Session->setFlash(__('Ocorreu algum erro.'), 'cms_msg', ['class' => 'danger'], 'cms');
		}
		return $this->redirect(['action' => 'index', 'cms' => true]);
	}
}