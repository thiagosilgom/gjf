<?= $this->element('cms_module_header') ?>

<!-- Main content -->
<section class="content">

  <div class="box">
    <div class="box-header with-border">
      <i class="fa fa-gear"></i>
      <h3 class="box-title">Configurações Gerais do Site</h3>
    </div><!-- /.box-header -->

    <?= $this->Form->create('Config', ['inputDefaults' => ['div' => ['class' => 'col-xs-12 col-md-6 form-group'], 'class' => 'form-control']]) ?>
      <div class="box-body">
        <div class="row">
          <div class="col-xs-12">
            <?= $this->Session->flash('cms') ?>
          </div>
        </div>

        <div class="margin">
          <div class="row">
            <?= $this->Form->input('titulo', ['label' => 'Título']) ?>
          </div>
          <div class="row">
            <?= $this->Form->input('email', ['label' => 'E-mail']) ?>
          </div>
          <div class="row">
            <?= $this->Form->input('telefone', ['label' => 'Telefone']) ?>
          </div>
          <div class="row">
            <?= $this->Form->input('endereco', ['label' => 'Endereço']) ?>
          </div>
          <div class="row">
            <?= $this->Form->input('texto_login', ['label' => 'Texto do Login']) ?>
          </div>
        </div>
      </div><!-- /.box-body -->

      <div class="box-footer">
        <?= $this->Form->input('id', ['type' => 'hidden']) ?>
        <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-lg btn-primary pull-right']) ?>
      </div>
    <?= $this->Form->end() ?>
  </div><!-- /.box -->

</section><!-- /.content -->