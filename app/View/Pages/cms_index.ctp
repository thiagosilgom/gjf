<?= $this->element('cms_module_header') ?>

<!-- Main content -->
<section class="content">

  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Sistema de Gerenciamento de Conteúdo</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
      <p>Este sistema é usado para criar, editar, gerenciar e publicar o conteúdo do site.</p>
      <p>Navegue pelo menu para alterar as páginas do site.</p>
    </div><!-- /.box-body -->
  </div><!-- /.box -->

</section><!-- /.content -->