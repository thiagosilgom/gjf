<?= $this->element('cms_module_header') ?>

<!-- Main content -->
<section class="content">

	<div class="box">
		<div class="box-header with-border">
			<i class="fa fa-plus"></i>
			<h3 class="box-title">Adicionar Fatura</h3>
		</div><!-- /.box-header -->

		<?= $this->Form->create('Fatura', ['inputDefaults' => ['div' => ['class' => 'col-xs-12 col-md-6 form-group'], 'class' => 'form-control']]) ?>
			<div class="box-body">
				<div class="row">
					<div class="col-xs-12">
						<?= $this->Html->link('<i class="fa fa-list"></i> Listagem', ['action' => 'index', 'cms' => true], ['escape' => false, 'class' => 'btn btn-app']) ?>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<?= $this->Session->flash('cms') ?>
					</div>
				</div>

				<div class="margin">
					<div class="row">
						<?= $this->Form->input('empresa_id', ['label' => 'Empresa', 'empty' => 'Selecione']) ?>
					</div>
					<div class="row">
						<?= $this->Form->input('titulo', ['label' => 'Título']) ?>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-6 form-group">
							<label for="FaturaData">Data</label>
							<?= $this->Form->input('data', ['label' => false, 'type' => 'text', 'div' => 'input-group', 'class' => 'form-control cms-datepicker', 'before' => '<div class="input-group-addon"><i class="fa fa-calendar"></i></div>', 'default' => date('d/m/Y')]) ?>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 form-group">
							<div class="checkbox">
								<label>
									<?= $this->Form->input('ativo', ['label' => false, 'div' => false, 'class' => false, 'type' => 'checkbox']) ?>
									Ativo
								</label>
							</div>
						</div>
					</div>
				</div>

				<div class="margin">
					<div class="row">
						<div class="col-xs-12">
							<?= $this->element('cms_msg', ['message' => 'Upload disponível somente na edição do registro.', 'close' => false]) ?>
						</div>
					</div>
				</div>
			</div><!-- /.box-body -->

			<div class="box-footer">
				<?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-lg btn-primary pull-right']) ?>
			</div>
		<?= $this->Form->end() ?>
	</div><!-- /.box -->

</section><!-- /.content -->