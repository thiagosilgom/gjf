<?php $sizes = getimagesize(WWW_ROOT.trim($media['file'], '/'));  ?>

<div class="item <?php if($thumbID && $media['id'] === $thumbID): ?>thumbnail<?php endif; ?>">

		<input type="hidden" value="<?php echo $media['position']; ?>" name="data[Media][<?php echo $media['id']; ?>]">

		<div class="visu"><?php echo $this->Html->image($media['icon']); ?></div>
		<?php echo basename($media['file']); ?>

		<div class="actions">
			<?php if($thumbID !== false && $media['id'] !== $thumbID && $media['type'] == 'pic'): ?>
				<?php echo $this->Html->link(__d("media", "Colocar a imagem como destaque"),array('action'=>'thumb',$media['id'])); ?> -
			<?php endif; ?>
			<?php echo $this->Html->link(__d('media',"Remover"),array('action'=>'delete',$media['id']),array('class'=>'del')); ?>
			<?php if ($editor): ?>
				<?php if ($media['type'] != 'pic'): ?>
					- <a href="" class="submit"><?php echo __d('media',"Inserir o link do artigo"); ?></a>
				<?php else: ?>
					- <a href="#" class="toggle"><?php echo __d('media',"Exibir"); ?></a>
				<?php endif ?>
			<?php endif ?>
		</div>
		<div class="expand">
			<table>
				<tr>
					<td style="width:140px"><?php echo $this->Html->image($media['file']);?></td>
					<td>
						<p><strong><?php echo __d('media',"Nome do arquivo"); ?> :</strong> <?php echo basename($media['file']); ?></p>
						<p><strong><?php echo __d('media',"Tamanho da imagem"); ?> :</strong> <?php echo $sizes[0].'x'.$sizes[1]; ?></p>
					</td>
				</tr>
			</table>
			<table>
				<tr>
					<td style="width:140px"><label><?php echo __d('media',"Título"); ?></label></td>
					<td><input class="title" name="title" type="text" value="<?php echo basename($media['file']); ?>"></td>
				</tr>
				<tr>
					<td style="width:140px"><label><?php echo __d('media',"Texto alternativo"); ?></label></td>
					<td><input class="alt" name="alt" type="text"></td>
				</tr>
				<tr>
					<td style="width:140px"><label><?php echo __d('media',"Link de destino"); ?></label></td>
					<td><input class="href" name="href" type="text" value="<?php echo $this->Html->url($media['file']); ?>"></td>
				</tr>
				<tr>
					<td style="width:140px"><label><?php echo __d('media',"Alinhamento"); ?></label></td>
					<td>
						<input type="radio" name="align-<?php echo $media['id']; ?>" class="align" id="align-none-<?php echo $media['id']; ?>" value="none" checked>
						<?php echo $this->Html->image('/media/img/align-none.png'); ?><label for="align-none-<?php echo $media['id']; ?>">Nenhum</label>

						<input type="radio" name="align-<?php echo $media['id']; ?>" class="align" id="align-left-<?php echo $media['id']; ?>" value="left">
						<?php echo $this->Html->image('/media/img/align-left.png'); ?><label for="align-left-<?php echo $media['id']; ?>">Esquerda</label>

						<input type="radio" name="align-<?php echo $media['id']; ?>" class="align" id="align-center-<?php echo $media['id']; ?>" value="center">
						<?php echo $this->Html->image('/media/img/align-center.png'); ?><label for="align-center-<?php echo $media['id']; ?>">Centro</label>

						<input type="radio" name="align-<?php echo $media['id']; ?>" class="align" id="align-right-<?php echo $media['id']; ?>" value="right">
						<?php echo $this->Html->image('/media/img/align-right.png'); ?><label for="align-right-<?php echo $media['id']; ?>">Direita</label>
					</td>
				</tr>
				<tr>
					<td style="width:140px"><input type="hidden" class="filetype" name="filetype" value="<?php echo $media['type']; ?>" /></td>
					<td>
						<p><a href="" class="submit"><?php echo __d('media',"Inserir no artigo"); ?></a> <?php echo $this->Html->link(__d('media',"Remover"),array('action'=>'delete',$media['id']),array('class'=>'del')); ?></p>
					</td>
				</tr>
				<input type="hidden" name="file" value="<?php echo $this->Html->url($media['file']); ?>" class="file">
			</table>
		</div>
</div>