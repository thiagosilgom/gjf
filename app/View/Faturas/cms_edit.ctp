<?= $this->element('cms_module_header') ?>

<!-- Main content -->
<section class="content">

	<div class="box">
		<div class="box-header with-border">
			<i class="fa fa-edit"></i>
			<h3 class="box-title">Editar Fatura</h3>
		</div><!-- /.box-header -->

		<?= $this->Form->create('Fatura', ['inputDefaults' => ['div' => ['class' => 'col-xs-12 col-md-6 form-group'], 'class' => 'form-control'], 'type' => 'file']) ?>
			<div class="box-body">
				<div class="row">
					<div class="col-xs-12">
						<?= $this->Html->link('<i class="fa fa-list"></i> Listagem', ['action' => 'index', 'cms' => true], ['escape' => false, 'class' => 'btn btn-app']) ?>
						<?=
							!$this->request->data('Fatura.ativo')
							? $this->Html->link('<i class="fa fa-check"></i> Ativar', ['action' => 'activate', 'cms' => true, $this->request->data('Fatura.id'), 1], ['escape' => false, 'class' => 'btn btn-app'])
							: $this->Html->link('<i class="fa fa-ban"></i> Desativar', ['action' => 'activate', 'cms' => true, $this->request->data('Fatura.id'), 0], ['escape' => false, 'class' => 'btn btn-app'])
						?>
						<?= $this->Html->link('<i class="fa fa-remove"></i> Remover', ['action' => 'remove', 'cms' => true, $this->request->data('Fatura.id')], ['escape' => false, 'class' => 'btn btn-app'], __('Deseja realmente remover este registro?')) ?>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<?= $this->Session->flash('cms') ?>
					</div>
				</div>

				<div class="margin">
					<div class="row">
						<?= $this->Form->input('empresa_id', ['label' => 'Empresa', 'empty' => 'Selecione']) ?>
					</div>
					<div class="row">
						<?= $this->Form->input('titulo', ['label' => 'Título']) ?>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-6 form-group">
							<label for="FaturaData">Data</label>
							<?= $this->Form->input('data', ['label' => false, 'type' => 'text', 'div' => 'input-group', 'class' => 'form-control cms-datepicker', 'before' => '<div class="input-group-addon"><i class="fa fa-calendar"></i></div>']) ?>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 form-group">
							<div class="checkbox">
								<label>
									<?= $this->Form->input('ativo', ['label' => false, 'div' => false, 'class' => false, 'type' => 'checkbox']) ?>
									Ativo
								</label>
							</div>
						</div>
					</div>
				</div>

				<div class="margin">
					<?php if ($this->request->data('Fatura.arquivo')) : ?>
						<div class="row">
							<div class="col-xs-12 col-md-6 form-group">
								<a class="btn btn-block btn-primary" href="<?= $this->webroot ?>arquivos/faturas/<?= $this->request->data('Fatura.arquivo') ?>" target="_blank">Ver arquivo atual</a>
							</div>
						</div>
					<?php endif; ?>
					<div class="row">
						<div class="col-xs-12 form-group">
							<label for="FaturaUpload">Upload</label>
							<input name="data[upload]" type="file" accept="application/pdf" id="FaturaUpload">
							<p class="help-block">Arquivo PDF.</p>
						</div>
					</div>
				</div>
			</div><!-- /.box-body -->

			<div class="box-footer">
				<?= $this->Form->input('id', ['type' => 'hidden']) ?>
				<?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-lg btn-primary pull-right']) ?>
			</div>
		<?= $this->Form->end() ?>
	</div><!-- /.box -->

</section><!-- /.content -->