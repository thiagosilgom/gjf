<?php
/**
 * Usuario Model
 *
 */
class Boleto extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'titulo';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = [
		'empresa_id' => [
			'notBlank' => [
				'rule'    => 'notBlank',
				'message' => 'A empresa não pode ser vazia.'
			]
		],
		'titulo' => [
			'notBlank' => [
				'rule'    => 'notBlank',
				'message' => 'O título não pode ser vazio.'
			]
		],
		'data' => [
			'notBlank' => [
				'rule'    => 'notBlank',
				'message' => 'A data não pode ser vazia.'
			]
		]
	];

	public $belongsTo = [
		'Empresa' => [
			'className'  => 'Empresa',
			'foreignKey' => 'empresa_id',
			'conditions' => '',
			'fields'     => ''
		]
	];

	public function prepareToSave($data) {
    	if (isset($data['Boleto']['data']) && !empty($data['Boleto']['data'])) {
    		$data['Boleto']['data'] = Util::inverte($data['Boleto']['data'], '/', '-');
    	}

    	return $data;
	}
}
