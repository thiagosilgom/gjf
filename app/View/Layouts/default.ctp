<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="<?= $this->webroot ?>css/site/all.min.css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

    <title><?= $this->fetch('title') ?></title>
	<?= $this->Html->meta('description', $this->fetch('description')) ?>

    <script src="<?= $this->webroot ?>js/site/all.min.js"></script>
    <script src="<?= $this->webroot ?>js/site/chosen/chosen.jquery.min.js"></script>
    <script src="https://use.fontawesome.com/a4f52dacd1.js"></script>

	<?= $this->fetch('meta') ?>
	<?= $this->fetch('css') ?>
</head>
<body class="animated fadeIn">
	<?= $this->fetch('content') ?>

	<?= $this->fetch('script') ?>
</body>
</html>