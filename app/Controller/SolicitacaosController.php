<?php
App::uses('AppController', 'Controller');

class SolicitacaosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = ['Paginator', 'Flash', 'Session'];

	public function beforeFilter() {
		parent::beforeFilter();
		if ($this->isCMS()) {
			$this->set('module_config', [
				'title'       => 'Solicitações',
				'description' => ''
			]);
		}
	}

/**
 * cms_index method
 *
 * @return void
 */
	public function cms_index() {
		$conditions = [];
		$this->request->data['Solicitacao'] = [];
		if (isset($this->request->query['empresa_id']) && !empty($this->request->query['empresa_id'])) {
			$conditions += ['Solicitacao.empresa_id' => $this->request->query['empresa_id']];
			$this->request->data['Solicitacao'] += ['empresa_id' => $this->request->query['empresa_id']];
		}
		if (isset($this->request->query['finalizado']) && in_array($this->request->query['finalizado'], ['0','1'])) {
			$conditions += ['Solicitacao.finalizado' => $this->request->query['finalizado']];
			$this->request->data['Solicitacao'] += ['finalizado' => $this->request->query['finalizado']];
		}
		if (isset($this->request->query['busca']) && !empty($this->request->query['busca'])) {
			$conditions += ['OR' => [
				'Solicitacao.id' => $this->request->query['busca'],
				'Solicitacao.titulo LIKE' => '%'.$this->request->query['busca'].'%',
				'Solicitacao.assunto LIKE' => '%'.$this->request->query['busca'].'%',
				'Solicitacao.mensagem LIKE' => '%'.$this->request->query['busca'].'%'
			]];
			$this->request->data['Solicitacao'] += ['busca' => $this->request->query['busca']];
		}
		$this->Paginator->settings['conditions'] = $conditions;
		$this->Paginator->settings['order'] = ['Solicitacao.data_ultima_mensagem' => 'desc', 'Solicitacao.id' => 'desc'];
		$this->Solicitacao->recursive = 0;
		$this->set('the_list', $this->Paginator->paginate());

		$empresas = $this->Solicitacao->Empresa->find('list');
		$this->set(compact('empresas'));
	}

/**
 * cms_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cms_view($id = null) {
		if (!$this->Solicitacao->exists($id)) {
			throw new NotFoundException(__('Requisição inválida.'));
		}
		if ($this->request->is(['post', 'put'])) {
			$now = date('Y-m-d H:i:s');
			$this->request->data('Resposta.created', $now);
			$this->request->data('Resposta.modified', $now);
			if ($this->Solicitacao->Resposta->save($this->request->data)) {
				if ($this->Solicitacao->save(['id' => $id, 'data_ultima_mensagem' => $now])) {
					$this->Session->setFlash(__('Registro atualizado com sucesso.'), 'cms_msg', ['class' => 'success'], 'cms');
					return $this->redirect(['action' => 'view', 'cms' => true, $id]);
				}
			}
			$this->Session->setFlash(__('Ocorreu algum erro.'), 'cms_msg', ['class' => 'danger'], 'cms');
		}
		
		$this->Solicitacao->unbindModel(['belongsTo' => ['Empresa']]);
		$this->Solicitacao->Cadastro->unbindModel(['belongsTo' => ['Empresa'], 'hasMany' => ['Solicitacao','Resposta']]);
		$this->Solicitacao->Resposta->unbindModel(['belongsTo' => ['Solicitacao']]);
		$solicitacao = $this->Solicitacao->find('first', [
			'conditions' => ['Solicitacao.id' => $id],
			'recursive' => 2
		]);
		$this->set(compact('solicitacao'));
	}

/**
 * cms_remove method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cms_remove($id = null) {
		$this->Solicitacao->id = $id;
		if (!$this->Solicitacao->exists()) {
			throw new NotFoundException(__('Registro não encontrado.'));
		}
		if ($this->Solicitacao->delete()) {
			$this->Session->setFlash(__('Registro removido com sucesso.'), 'cms_msg', ['class' => 'success'], 'cms');
		} else {
			$this->Session->setFlash(__('Ocorreu algum erro.'), 'cms_msg', ['class' => 'danger'], 'cms');
		}
		return $this->redirect(['action' => 'index', 'cms' => true]);
	}

/**
 * cms_finalizar method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cms_finalizar($id = null, $true = 1) {
		$this->Solicitacao->id = $id;
		if (!$this->Solicitacao->exists()) {
			throw new NotFoundException(__('Registro não encontrado.')); 
		}
		if ($this->Solicitacao->saveField('finalizado', (int) $true)) {
			$this->Session->setFlash(__('Registro atualizado com sucesso.'), 'cms_msg', ['class' => 'success'], 'cms');
		} else {
			$this->Session->setFlash(__('Ocorreu algum erro.'), 'cms_msg', ['class' => 'danger'], 'cms');
		}
		return $this->redirect($this->referer());
	}
}