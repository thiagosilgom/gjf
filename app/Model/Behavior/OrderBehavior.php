<?php
class OrderBehavior extends ModelBehavior
{
    /**
     * Guarda as configurações
     *
     * @var     array $settings
     */
    public $settings = [];

    /**
     * Guarda as configurações para execução do comportamento
     *
     * @param   Model $Model
     * @param   array $settings
     * @return  void
     */
    public function setUp(Model $model, $config = [])
    {
        $this->settings[$model->name] = array_merge(
            [
                'field' => 'ordem',
                'group' => []
            ],
            $config
        );
    }

    /**
     * Método que prepara os dados para adição e edição de ordem
     *
     * @param   Model $Model
     * @param   array $options
     * @return  bool
     */
    public function beforeSave(Model $Model, $options = [])
    {
        $field      = $this->settings[$Model->name]['field'];
        $conditions = $this->getConditions($Model);
        $data       = $Model->data;
        // valor atual
        if (
            isset($data[$Model->alias][$Model->primaryKey]) &&
            !empty($data[$Model->alias][$Model->primaryKey])
        ) {
            $atual = $Model->read(null, $data[$Model->alias][$Model->primaryKey]); 
        } else { // maior mais um
            $atual = [
                $Model->alias => [
                    $field => $Model->find('count', ['conditions' => $conditions]) + 1,
                ]
            ];
        }
        $higher = $Model->find('count', ['conditions' => $conditions]);
        // setar para ultimo
        if (empty($data[$Model->alias][$field]) || $data[$Model->alias][$field] > ($higher+1)) {
            $data[$Model->alias][$field] = $higher + (@$data[$Model->alias][$Model->primaryKey] ? 0 : 1);
        }
        return $this->updateData($Model, $data, $atual);
    }

    /**
     * Método que prepara dados para exclusão de um item
     *
     * @param   Model $Model 
     * @return  void
     */
    public function beforeDelete(Model $Model, $cascade = true)
    {
        $field      = $this->settings[$Model->name]['field'];
        $conditions = $this->settings[$Model->name]['group'];

        $data[$Model->alias][$Model->primaryKey] = $Model->id;
        $data[$Model->alias][$field] = $Model->find('count', ['conditions' => $conditions]);
        
        $atual      = $Model->read();
        return $this->updateData($Model, $data, $atual);
    }

    /**
     * Filtra os parâmetros passados tornado-os condicionais de fato
     *
     * @param   Model $Model
     * @todo    Estender a forma como parâmetros são passados (>, <, between, etc)
     * @return  array|null
     */
    protected function getConditions($Model)
    {
        $conditions = $this->settings[$Model->name]['group'];
        if (empty($conditions)) {
            return [];
        }
        $return = [];
        foreach ($conditions as $index => $cond) {
            if (is_numeric($index)) {
                $return["{$Model->alias}.{$cond}"] = @$Model->data[$Model->name][$cond];                
            } else {
                $return[$index] = $cond;
            }
        }
        return $return;
    }
    /**
     * Método protegido que faz de fato as movimentações necessárias para manutenção da ordenação
     *
     * @param   Model $Model 
     * @param   array $data
     * @param   array $atual
     * @return  bool
     */
    protected function updateData(&$Model, $data, $atual)
    {
        $field      = $this->settings[$Model->name]['field'];
        $conditions = $this->getConditions($Model);
        if ($data[$Model->alias][$field] > $atual[$Model->alias][$field]) { // aumentando prioridade
            $All = $Model->find(
                'all',
                [
                    'conditions' => array_merge($conditions, [
                        "{$Model->alias}.{$field} BETWEEN ? AND ?" => [$atual[$Model->alias][$field], $data[$Model->alias][$field]],
                        "{$Model->alias}.{$Model->primaryKey} !="  => (int) @$data[$Model->alias][$Model->primaryKey]
                        ]
                    )
                ]
            );
            $increment = -1;
        } else { // diminuindo prioridade
            $All = $Model->find(
                'all',
                [
                    'conditions' => array_merge(
                        $conditions, 
                        [
                            "{$Model->alias}.{$field} BETWEEN ? AND ?" => [$data[$Model->alias][$field], $atual[$Model->alias][$field]],
                            "{$Model->alias}.{$Model->primaryKey} !="  => (int) @$data[$Model->alias][$Model->primaryKey]
                        ]
                    )
                ]
            );
            $increment = 1;
        }
        $Model->Behaviors->unload('Order');
        foreach ($All as $item) {
            $item[$Model->alias][$field] += $increment;
            $Model->save($item);
        }
        if (!isset($data[$Model->alias][$Model->primaryKey])) {
            $Model->create();
        }
        $Model->set($data); // restaura dados sendo salvos
        return true;
    }

    /**
     * Retorna a base para criação de select
     *
     * @param   Model $Model
     * @return  array
     */
    public function getOrder($Model)
    {
        $field      = $this->settings[$Model->name]['field'];
        $conditions = $this->getConditions($Model);
        if ($conditions) {
            if ($Model->id) { // not effective way
                $data['options'] =  $Model->find('list', ['conditions' => $conditions, 'fields' => [$field, $field], 'order' => "{$field} asc"]);
            } else {
                $data['type'] = 'hidden';
                $data['value']  = '';
            }
        } else {
            $data['options']  =  $Model->find('list', ['fields' => [$field, $field], 'order' => "{$field} asc"]);  
            if (!$Model->id) {
                $data['options'][count($data['options'])+1] = count($data['options'])+1;
            }
            $data['empty'] = 'Selecione';
        }
        return $data;
    }
};