<?php
App::uses('AppController', 'Controller');

class ConfigsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = ['Paginator', 'Flash', 'Session'];

	public function beforeFilter() {
		parent::beforeFilter();
		if ($this->isCMS()) {
			$this->set('module_config', [
				'title'       => 'Configurações',
				'description' => ''
			]);
		}
	}

/**
 * cms_index method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cms_index($id = 1) {
		if (!$this->Config->exists($id)) {
			throw new NotFoundException(__('Requisição inválida.'));
		}
		if ($this->request->is(['post', 'put'])) {
			if ($this->Config->save($this->request->data)) {
				$this->Session->setFlash(__('Registro atualizado com sucesso.'), 'cms_msg', ['class' => 'success'], 'cms');
				return $this->redirect(['action' => 'index', 'cms' => true]);
			} else {
				$this->Session->setFlash(__('Ocorreu algum erro.'), 'cms_msg', ['class' => 'danger'], 'cms');
			}
		} else {
			$this->request->data = $this->Config->read(null, $id);
		}
	}
}