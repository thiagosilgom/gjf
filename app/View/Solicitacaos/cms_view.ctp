<?= $this->element('cms_module_header') ?>

<!-- Main content -->
<section class="content">

	<div class="box">
		<div class="box-header with-border">
			<i class="fa fa-wechat"></i>
			<h3 class="box-title">Responder Solicitação</h3>
		</div><!-- /.box-header -->

		<?= $this->Form->create('Resposta', ['inputDefaults' => ['div' => ['class' => 'col-xs-12 form-group'], 'class' => 'form-control']]) ?>
			<div class="box-body">
				<div class="row">
					<div class="col-xs-12">
						<?= $this->Html->link('<i class="fa fa-list"></i> Listagem', ['action' => 'index', 'cms' => true], ['escape' => false, 'class' => 'btn btn-app']) ?>
						<?=
							!$solicitacao['Solicitacao']['finalizado']
							? $this->Html->link('<i class="fa fa-check"></i> Finalizar', ['action' => 'finalizar', 'cms' => true, $solicitacao['Solicitacao']['id'], 1], ['escape' => false, 'class' => 'btn btn-app'])
							: $this->Html->link('<i class="fa fa-question"></i> Deixar em Aberto', ['action' => 'finalizar', 'cms' => true, $solicitacao['Solicitacao']['id'], 0], ['escape' => false, 'class' => 'btn btn-app'])
						?>
						<?= $this->Html->link('<i class="fa fa-remove"></i> Remover', ['action' => 'remove', 'cms' => true, $solicitacao['Solicitacao']['id']], ['escape' => false, 'class' => 'btn btn-app'], __('Deseja realmente remover este registro?')) ?>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<?= $this->Session->flash('cms') ?>
					</div>
				</div>

				<div class="margin">
					<div class="row">
						<div class="col-xs-12 user-block">
							<h3><?= Util::codigo($solicitacao['Solicitacao']['id']) ?> - <?= $solicitacao['Solicitacao']['titulo'] ?></h3>
							<p><?= nl2br($solicitacao['Solicitacao']['mensagem']) ?></p>
							<span class="username"><?= (isset($solicitacao['Cadastro']['nome']) ? $solicitacao['Cadastro']['nome'] : '') ?></span>
							<span class="description"><?= (!empty($solicitacao['Solicitacao']['created']) ? date('d/m/Y H:i:s', strtotime($solicitacao['Solicitacao']['created'])) : '') ?></span>
						</div>
					</div>
				</div>
				<hr>
				<?php foreach ($solicitacao['Resposta'] as $resposta) : ?>
					<div class="margin">
						<div class="row">
							<div class="col-xs-12 user-block <?= ($resposta['cms'] ? 'text-right' : '') ?>">
								<p><?= nl2br($resposta['mensagem']) ?></p>
								<span class="username"><?= (isset($resposta[$resposta['cms'] ? 'Usuario' : 'Cadastro']['nome']) ? $resposta[$resposta['cms'] ? 'Usuario' : 'Cadastro']['nome'] : '') ?></span>
								<span class="description"><?= (!empty($resposta['created']) ? date('d/m/Y H:i:s', strtotime($resposta['created'])) : '') ?></span>
							</div>
						</div>
					</div>
					<hr>
				<?php endforeach; ?>
				<div class="margin">
					<div class="row">
						<div class="col-xs-12">
							<?= $this->Form->input('mensagem', ['type' => 'textarea', 'placeholder' => 'Digite aqui sua resposta...']) ?>
						</div>
					</div>
				</div>
			</div><!-- /.box-body -->

			<div class="box-footer">
				<?= $this->Form->input('solicitacao_id', ['type' => 'hidden', 'value' => $solicitacao['Solicitacao']['id']]) ?>
				<?= $this->Form->input('usuario_id', ['type' => 'hidden', 'value' => $the_user['Usuario']['id']]) ?>
				<?= $this->Form->input('cms', ['type' => 'hidden', 'value' => 1]) ?>
				<?= $this->Form->button(__('Responder'), ['class' => 'btn btn-lg btn-primary pull-right']) ?>
			</div>
		<?= $this->Form->end() ?>
	</div><!-- /.box -->

</section><!-- /.content -->
<style>
	.user-block span{margin-right:50px;}
</style>