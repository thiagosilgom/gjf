<?= $this->element('cms_module_header') ?>

<!-- Main content -->
<section class="content">

  <div class="box">
    <div class="box-header with-border">
      <i class="fa fa-list"></i>
      <h3 class="box-title">Listagem de Páginas</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
      <div class="row">
        <div class="col-xs-12 col-md-6">
          <?= $this->Form->create('Pagina', ['inputDefaults' => ['div' => false, 'label' => false], 'type' => 'get', 'url' => ['action' => 'index', 'cms' => true]]) ?>
            <div class="input-group margin">
              <?= $this->Form->input('busca', ['placeholder' => __('Digite o título...'), 'class' => 'form-control']) ?>
              <span class="input-group-btn">
                <?= $this->Form->button(__('Buscar'), ['class' => 'btn btn-info btn-flat']) ?>
              </span>
            </div>
          <?= $this->Form->end() ?>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-12">
          <?= $this->Session->flash('cms') ?>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-12">
          <?php if (empty($the_list)) : ?>
            <?= $this->element('cms_msg', ['message' => __('Nenhum registro encontrado.'), 'class' => 'warning', 'close' => false]) ?>
          <?php else : ?>
            <div class="margin">
              <div class="box-body table-responsive no-padding">
                <table class="table table-hover table-striped">
                  <tbody>
                    <tr>
                      <th class="hidden-xs">ID</th>
                      <th>Título</th>
                      <th class="hidden-xs">Ações</th>
                    </tr>
                    <?php foreach ($the_list as $item) : ?>
                      <tr>
                        <td class="hidden-xs"><?= $this->Html->link($item['Pagina']['id'], ['action' => 'edit', 'cms' => true, $item['Pagina']['id']], ['class' => 'text-black']) ?></td>
                        <td><?= $this->Html->link($item['Pagina']['titulo'], ['action' => 'edit', 'cms' => true, $item['Pagina']['id']], ['class' => 'text-black']) ?></td>
                        <td class="hidden-xs">
                          <?= $this->Html->link('<i class="fa fa-edit text-black"></i>', ['action' => 'edit', 'cms' => true, $item['Pagina']['id']], ['escape' => false, 'class' => 'link-acao']) ?>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          <?php endif; ?>
        </div>
      </div>

      <?= $this->element('cms_paginacao') ?>
    </div><!-- /.box-body -->
  </div><!-- /.box -->

</section><!-- /.content -->