<?php
/**
 * Usuario Model
 *
 */
class Fatura extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'titulo';

	public $virtualFields = ['mes' => "DATE_FORMAT(Fatura.data, '%Y-%m')"];

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = [
		'empresa_id' => [
			'notBlank' => [
				'rule'    => 'notBlank',
				'message' => 'A empresa não pode ser vazia.'
			]
		],
		'titulo' => [
			'notBlank' => [
				'rule'    => 'notBlank',
				'message' => 'O título não pode ser vazio.'
			]
		],
		'data' => [
			'notBlank' => [
				'rule'    => 'notBlank',
				'message' => 'A data não pode ser vazia.'
			]
		]
	];

	public $belongsTo = [
		'Empresa' => [
			'className'  => 'Empresa',
			'foreignKey' => 'empresa_id',
			'conditions' => '',
			'fields'     => ''
		]
	];

	public function prepareToSave($data) {
    	if (isset($data['Fatura']['data']) && !empty($data['Fatura']['data'])) {
    		$data['Fatura']['data'] = Util::inverte($data['Fatura']['data'], '/', '-');
    	}

    	return $data;
	}
}
