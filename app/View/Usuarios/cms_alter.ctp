<?= $this->element('cms_module_header') ?>

<!-- Main content -->
<section class="content">

  <div class="box">
    <div class="box-header with-border">
      <i class="fa fa-lock"></i>
      <h3 class="box-title">Alterar Senha de Usuário</h3>
    </div><!-- /.box-header -->

    <?= $this->Form->create('Usuario', ['inputDefaults' => ['div' => ['class' => 'col-xs-12 col-md-6 form-group'], 'class' => 'form-control']]) ?>
      <div class="box-body">
        <div class="row">
          <div class="col-xs-12">
            <?= $this->Html->link('<i class="fa fa-list"></i> Listagem', ['action' => 'index', 'cms' => true], ['escape' => false, 'class' => 'btn btn-app']) ?>
            <?= $this->Html->link('<i class="fa fa-edit"></i> Editar', ['action' => 'edit', 'cms' => true, $this->request->data('Usuario.id')], ['escape' => false, 'class' => 'btn btn-app']) ?>
            <?= $this->Html->link('<i class="fa fa-remove"></i> Remover', ['action' => 'remove', 'cms' => true, $this->request->data('Usuario.id')], ['escape' => false, 'class' => 'btn btn-app'], __('Deseja realmente remover este registro?')) ?>
          </div>
        </div>

        <div class="row">
          <div class="col-xs-12">
            <?= $this->Session->flash('cms') ?>
          </div>
        </div>

        <div class="margin">
          <div class="row">
            <?= $this->Form->input('login', ['label' => 'Login', 'readonly' => 'readonly']) ?>
          </div>
          <div class="row">
            <?= $this->Form->input('senha', ['label' => 'Senha', 'type' => 'password']) ?>
          </div>
        </div>
      </div><!-- /.box-body -->

      <div class="box-footer">
        <?= $this->Form->input('id', ['type' => 'hidden']) ?>
        <?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-lg btn-primary pull-right']) ?>
      </div>
    <?= $this->Form->end() ?>
  </div><!-- /.box -->

</section><!-- /.content -->