-- --------------------------------------------------------
-- Servidor:                     localhost
-- Versão do servidor:           5.6.17 - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela cakecms.configs
CREATE TABLE IF NOT EXISTS `configs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `titulo` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  `endereco` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Copiando dados para a tabela cakecms.configs: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `configs` DISABLE KEYS */;
INSERT INTO `configs` (`id`, `created`, `modified`, `titulo`, `email`, `telefone`, `endereco`) VALUES
	(1, NULL, NULL, 'CakeCMS', 'contato@contato.com', NULL, NULL);
/*!40000 ALTER TABLE `configs` ENABLE KEYS */;

-- Copiando estrutura para tabela cakecms.medias
CREATE TABLE IF NOT EXISTS `medias` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ref` varchar(60) DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ref` (`ref`),
  KEY `ref_id` (`ref_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela cakecms.medias: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `medias` DISABLE KEYS */;
/*!40000 ALTER TABLE `medias` ENABLE KEYS */;

-- Copiando estrutura para tabela cakecms.midias
CREATE TABLE IF NOT EXISTS `midias` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `titulo` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Copiando dados para a tabela cakecms.midias: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `midias` DISABLE KEYS */;
INSERT INTO `midias` (`id`, `created`, `modified`, `titulo`) VALUES
	(1, NULL, NULL, 'Mídias do Editor de Texto');
/*!40000 ALTER TABLE `midias` ENABLE KEYS */;

-- Copiando estrutura para tabela cakecms.paginas
CREATE TABLE IF NOT EXISTS `paginas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `titulo` varchar(255) NOT NULL,
  `texto` text,
  `seo_title` text,
  `seo_description` text,
  `tem_texto` tinyint(4) DEFAULT NULL,
  `tem_editor` tinyint(4) DEFAULT NULL,
  `tem_midias` tinyint(4) DEFAULT NULL,
  `tem_seo` tinyint(4) DEFAULT NULL,
  `ativo` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Copiando dados para a tabela cakecms.paginas: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `paginas` DISABLE KEYS */;
INSERT INTO `paginas` (`id`, `created`, `modified`, `titulo`, `texto`, `seo_title`, `seo_description`, `tem_texto`, `tem_editor`, `tem_midias`, `tem_seo`, `ativo`) VALUES
	(1, NULL, NULL, 'Home', NULL, NULL, NULL, 0, 0, 0, 1, 1);
/*!40000 ALTER TABLE `paginas` ENABLE KEYS */;

-- Copiando estrutura para tabela cakecms.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `login` varchar(255) DEFAULT NULL,
  `senha` varchar(255) DEFAULT NULL,
  `ativo` tinyint(4) DEFAULT '1',
  `media_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  KEY `fk_usuarios_media_id` (`media_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Copiando dados para a tabela cakecms.usuarios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id`, `created`, `modified`, `nome`, `email`, `login`, `senha`, `ativo`, `media_id`) VALUES
	(1, NULL, '2019-01-28 14:28:37', 'Admin', '', 'admin', '$2a$10$E.CKeE3qMPcRCgx2AnGTWeeBt8vGwzIFER8CTCyAvPFOC0bWzwh96', 1, NULL);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

-- Copiando estrutura para tabela cakecms._tocopy
CREATE TABLE IF NOT EXISTS `_tocopy` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `categoria_id` int(11) unsigned DEFAULT NULL,
  `titulo` varchar(255) NOT NULL,
  `data` date DEFAULT NULL,
  `texto` text,
  `link` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `ativo` tinyint(4) DEFAULT NULL,
  `destaque` tinyint(4) DEFAULT NULL,
  `seo_title` text,
  `seo_description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela cakecms._tocopy: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `_tocopy` DISABLE KEYS */;
/*!40000 ALTER TABLE `_tocopy` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
