<?php
App::uses('AppController', 'Controller');

class CadastrosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = ['Paginator', 'Flash', 'Session'];

	public function beforeFilter() {
		parent::beforeFilter();
		if ($this->isCMS()) {
			$this->set('module_config', [
				'title'       => 'Usuários de Empresas',
				'description' => ''
			]);
		}
	}

/**
 * cms_index method
 *
 * @return void
 */
	public function cms_index() {
		$conditions = [];
		$this->request->data['Cadastro'] = [];
		if (isset($this->request->query['empresa_id']) && !empty($this->request->query['empresa_id'])) {
			$conditions += ['Cadastro.empresa_id' => $this->request->query['empresa_id']];
			$this->request->data['Cadastro'] += ['empresa_id' => $this->request->query['empresa_id']];
		}
		if (isset($this->request->query['busca']) && !empty($this->request->query['busca'])) {
			$conditions += ['OR' => [
				'Cadastro.nome LIKE' => '%'.$this->request->query['busca'].'%',
				'Cadastro.email LIKE' => '%'.$this->request->query['busca'].'%',
				'Cadastro.telefone LIKE' => '%'.$this->request->query['busca'].'%'
			]];
			$this->request->data['Cadastro'] += ['busca' => $this->request->query['busca']];
		}
		$this->Paginator->settings['conditions'] = $conditions;
		$this->Paginator->settings['order'] = ['Cadastro.id' => 'desc'];
		$this->Cadastro->recursive = 0;
		$this->set('the_list', $this->Paginator->paginate());

		$empresas = $this->Cadastro->Empresa->find('list');
		$this->set(compact('empresas'));
	}

/**
 * cms_add method
 *
 * @return void
 */
public function cms_add() {
	$this->Cadastro->create();
	if ($this->request->is(['post', 'put'])) {
		$senha = Util::hash();
			$this->request->data('Cadastro.senha', md5($senha));
			if ($this->Cadastro->save($this->request->data)) {
				$this->loadModel('Config');
				$configs = $this->Config->read(null, 1);
				$cadastro = $this->Cadastro->find('first', [
					'conditions' => ['Cadastro.id' => $this->Cadastro->id],
					'recursive' => 0
				]);
				$send = Util::email([
					'subject' => 'GJF - Cadastro no Sistema',
					'title' => '',
					'subtitle' => '',
					'fields' => [
						'Empresa' => $cadastro['Empresa']['titulo'],
						'Nome'    => $cadastro['Cadastro']['nome'],
						'Login'   => $cadastro['Cadastro']['email'],
						'Senha'   => $senha
					],
					'to' => $cadastro['Cadastro']['email']
				], $configs);
				if ($send == 1) {
					$this->Session->setFlash(__('Um e-mail foi enviado para %s com a senha.', $cadastro['Cadastro']['email']), 'cms_msg', ['class' => 'info'], 'cms');
				} else {
					$this->Session->setFlash(__('Não foi possível enviar o e-mail de cadastro para %s.', $cadastro['Cadastro']['email']), 'cms_msg', ['class' => 'warning'], 'cms');
				}
				return $this->redirect(['cms' => true, 'action' => 'edit', $this->Cadastro->id]);
			} else {
				$this->Session->setFlash(__('Ocorreu algum erro.'), 'cms_msg', ['class' => 'danger'], 'cms');
			}
		}
		
		$empresas = $this->Cadastro->Empresa->find('list');
		$this->set(compact('empresas'));
	}

/**
 * cms_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cms_edit($id = null) {
		if (!$this->Cadastro->exists($id)) {
			throw new NotFoundException(__('Requisição inválida.'));
		}
		if ($this->request->is(['post', 'put'])) {
			if ($this->request->data('Cadastro.senha')) {
				$this->request->data('Cadastro.senha', md5($this->request->data('Cadastro.senha')));
			} else {
				unset($this->request->data['Cadastro']['senha']);
			}
			if ($this->Cadastro->save($this->request->data)) {
				$this->Session->setFlash(__('Registro atualizado com sucesso.'), 'cms_msg', ['class' => 'success'], 'cms');
				return $this->redirect(['action' => 'edit', 'cms' => true, $id]);
			} else {
				$this->Session->setFlash(__('Ocorreu algum erro.'), 'cms_msg', ['class' => 'danger'], 'cms');
			}
		} else {
			$this->request->data = $this->Cadastro->read(null, $id);
		}
		unset($this->request->data['Cadastro']['senha']);

		$empresas = $this->Cadastro->Empresa->find('list');
		$this->set(compact('empresas'));
	}

/**
 * cms_remove method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cms_remove($id = null) {
		$this->Cadastro->id = $id;
		if (!$this->Cadastro->exists()) {
			throw new NotFoundException(__('Registro não encontrado.'));
		}
		if ($this->Cadastro->delete()) {
			$this->Session->setFlash(__('Registro removido com sucesso.'), 'cms_msg', ['class' => 'success'], 'cms');
		} else {
			$this->Session->setFlash(__('Ocorreu algum erro.'), 'cms_msg', ['class' => 'danger'], 'cms');
		}
		return $this->redirect(['action' => 'index', 'cms' => true]);
	}

/**
 * cms_activate method
 *
 * @throws NotFoundException
 * @param string $id
 * @param integer $true
 * @return void
 */
	public function cms_activate($id = null, $true = 1) {
		$this->Cadastro->id = $id;
		if (!$this->Cadastro->exists()) {
			throw new NotFoundException(__('Registro não encontrado.')); 
		}
		if ($this->Cadastro->saveField('ativo', (int) $true)) {
			$this->Session->setFlash(__('Registro atualizado com sucesso.'), 'cms_msg', ['class' => 'success'], 'cms');
		} else {
			$this->Session->setFlash(__('Ocorreu algum erro.'), 'cms_msg', ['class' => 'danger'], 'cms');
		}
		return $this->redirect($this->referer());
	}
}