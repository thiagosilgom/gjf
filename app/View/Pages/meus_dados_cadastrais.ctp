<?= $this->element('header') ?>

<div class="container-fluid">
    <div class="row">
        <?= $this->element('sidebar', ['active' => 'meus_dados_cadastrais']) ?>

        <div class="col-lg-9 col-md-12 h100">
            <div class="mt160"></div>
            <div class="content-conteudo">
                <h1>Meus dados cadastrais</h1>
                <div class="mt50"></div>
				<?= $this->Session->flash('sistema') ?>
				<?= $this->Form->create('Cadastro', ['inputDefaults' => ['div' => false, 'label' => false]]) ?>
					<label for="CadastroNome">Nome</label>
					<div class="mt5"></div>
					<?= $this->Form->input('nome', ['type' => 'text']) ?>
					<div class="mt30"></div>
					<label for="CadastroEmail">E-mail</label>
					<div class="mt5"></div>
					<?= $this->Form->input('email', ['type' => 'email']) ?>
					<div class="mt30"></div>
					<label for="CadastroTelefone">Telefone</label>
					<div class="mt5"></div>
					<?= $this->Form->input('telefone', ['type' => 'text']) ?>
					<div class="mt30"></div>
					<label for="CadastroNovaSenha">Nova Senha</label>
					<div class="mt5"></div>
					<?= $this->Form->input('nova_senha', ['type' => 'password', 'placeholder' => 'Deixe em branco, caso não queira alterar a senha']) ?>
					<div class="mt30"></div>
					<label for="CadastroConfirmacaoSenha">Confirmação de Senha</label>
					<div class="mt5"></div>
					<?= $this->Form->input('confirmacao_senha', ['type' => 'password', 'placeholder' => 'Deixe em branco, caso não queira alterar a senha']) ?>
					<div class="mt30"></div>
					<?= $this->Form->input('editar cadastro', ['type' => 'submit']) ?>
					<div class="mt30"></div>
				<?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        function h100() {
            $('.h100').css('height', window.innerHeight);
        }h100();

        $(window).load(h100).resize(h100);
    })
</script>