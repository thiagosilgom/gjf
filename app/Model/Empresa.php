<?php
/**
 * Usuario Model
 *
 */
class Empresa extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'titulo';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = [
		'titulo' => [
			'notBlank' => [
				'rule'    => 'notBlank',
				'message' => 'O título não pode ser vazio.'
			]
		]
	];

	public $hasMany = [
		'Cadastro' => array(
			'className'  => 'Cadastro',
			'foreignKey' => 'empresa_id',
			'dependent'  => true,
			'conditions' => '',
			'fields'     => '',
			'order'      => ''
		),
		'Boleto' => array(
			'className'  => 'Boleto',
			'foreignKey' => 'empresa_id',
			'dependent'  => true,
			'conditions' => '',
			'fields'     => '',
			'order'      => ''
		),
		'Fatura' => array(
			'className'  => 'Fatura',
			'foreignKey' => 'empresa_id',
			'dependent'  => true,
			'conditions' => '',
			'fields'     => '',
			'order'      => ''
		),
		'Solicitacao' => array(
			'className'  => 'Solicitacao',
			'foreignKey' => 'empresa_id',
			'dependent'  => true,
			'conditions' => '',
			'fields'     => '',
			'order'      => ''
		)
	];
}
