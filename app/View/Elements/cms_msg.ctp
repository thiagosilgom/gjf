<?php
	$class  = (!isset($class)  ? 'info' : $class);
	$margin = (!isset($margin) ? true   : false);
	$close  = (!isset($close)  ? true   : false);
?>
<div class="<?= ($margin ? 'margin' : '') ?> alert alert-<?= $class ?> <?= ($close ? 'alert-dismissable' : '') ?>">
  <?php if ($close) : ?>
  	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <?php endif; ?>
  <?= $message ?>
</div>