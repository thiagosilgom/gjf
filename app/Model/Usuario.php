<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
/**
 * Usuario Model
 *
 */
class Usuario extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nome';

	public $actsAs = ['Media.Media'];

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = [
		'login' => [
			'notBlank' => [
				'rule'    => 'notBlank',
				'message' => 'Usuário deverá ter um login.'
			],
			'isUnique' => [
				'rule'    => 'isUnique',
				'message' => 'Este usuário já existe.'
			]
		],
		'senha' => [
			'notBlank' => [
				'rule'    => 'notBlank',
				'message' => 'A senha não pode ser vazia.'
			]
		]
	];

	public function beforeSave($options = []) {
        $field = 'senha';
        if (isset($this->data[$this->alias][$field]) && !empty($this->data[$this->alias][$field])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias][$field] = $passwordHasher->hash($this->data[$this->alias][$field]);
        }
        return true;
    }
}
