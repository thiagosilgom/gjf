<?php
App::uses('AppController', 'Controller');

class PaginasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = ['Paginator', 'Flash', 'Session'];

	public function beforeFilter() {
		parent::beforeFilter();
		if ($this->isCMS()) {
			$this->set('module_config', [
				'title'       => 'Páginas',
				'description' => 'seções e blocos'
			]);
		}
	}

/**
 * cms_index method
 *
 * @return void
 */
	public function cms_index() {
		$conditions = ['Pagina.ativo' => true];
		if (isset($this->request->query['busca']) && !empty($this->request->query['busca'])) {
			$conditions += ['Pagina.titulo LIKE' => '%'.$this->request->query['busca'].'%'];
			$this->request->data['Pagina'] = ['busca' => $this->request->query['busca']];
		}
		$this->Paginator->settings['conditions'] = $conditions;
		$this->Paginator->settings['order'] = ['Pagina.id' => 'asc'];
		$this->Pagina->recursive = 0;
		$this->set('the_list', $this->Paginator->paginate());
	}

/**
 * cms_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cms_edit($id = null) {
		if (!$this->Pagina->exists($id)) {
			throw new NotFoundException(__('Requisição inválida.'));
		}
		if ($this->request->is(['post', 'put'])) {
			if ($this->Pagina->save($this->request->data)) {
				$this->Session->setFlash(__('Registro atualizado com sucesso.'), 'cms_msg', ['class' => 'success'], 'cms');
				return $this->redirect(['action' => 'edit', 'cms' => true, $id]);
			} else {
				$this->Session->setFlash(__('Ocorreu algum erro.'), 'cms_msg', ['class' => 'danger'], 'cms');
			}
		} else {
			$this->request->data = $this->Pagina->read(null, $id);
		}
	}
}