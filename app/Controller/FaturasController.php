<?php
App::uses('AppController', 'Controller');

class FaturasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = ['Paginator', 'Flash', 'Session'];

	public function beforeFilter() {
		parent::beforeFilter();
		if ($this->isCMS()) {
			$this->set('module_config', [
				'title'       => 'Faturas',
				'description' => ''
			]);
		}
	}

/**
 * cms_index method
 *
 * @return void
 */
	public function cms_index() {
		$conditions = [];
		$this->request->data['Fatura'] = [];
		if (isset($this->request->query['empresa_id']) && !empty($this->request->query['empresa_id'])) {
			$conditions += ['Fatura.empresa_id' => $this->request->query['empresa_id']];
			$this->request->data['Fatura'] += ['empresa_id' => $this->request->query['empresa_id']];
		}
		if (isset($this->request->query['busca']) && !empty($this->request->query['busca'])) {
			$conditions += ['Fatura.titulo LIKE' => '%'.$this->request->query['busca'].'%'];
			$this->request->data['Fatura'] += ['busca' => $this->request->query['busca']];
		}
		$this->Paginator->settings['conditions'] = $conditions;
		$this->Paginator->settings['order'] = ['Fatura.data' => 'desc', 'Fatura.id' => 'desc'];
		$this->Fatura->recursive = 0;
		$this->set('the_list', $this->Paginator->paginate());

		$empresas = $this->Fatura->Empresa->find('list');
		$this->set(compact('empresas'));
	}

/**
 * cms_add method
 *
 * @return void
 */
	public function cms_add() {
		$this->Fatura->create();
		if ($this->request->is(['post', 'put'])) {
			try {
				if ($this->Fatura->saveAll($this->Fatura->prepareToSave($this->request->data))) {
					$this->Session->setFlash(__('Registro salvo com sucesso.'), 'cms_msg', ['class' => 'success'], 'cms');
					return $this->redirect(['cms' => true, 'action' => 'edit', $this->Fatura->id]);
				} else {
					$this->Session->setFlash(__('Ocorreu algum erro.'), 'cms_msg', ['class' => 'danger'], 'cms');
				}
			} catch(Exception $e) {
				$this->Session->setFlash(__('Ocorreu algum erro.'), 'cms_msg', ['class' => 'danger'], 'cms');
			}
		}

		$empresas = $this->Fatura->Empresa->find('list');
		$this->set(compact('empresas'));
	}

/**
 * cms_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cms_edit($id = null) {
		if (!$this->Fatura->exists($id)) {
			throw new NotFoundException(__('Requisição inválida.'));
		}
		$registro = $this->Fatura->read(null, $id);
		if ($this->request->is(['post', 'put'])) {
			try {
				$success = true;
				if ($this->request->data('upload') && $this->request->data('upload.name')) {
					if ($this->request->data('upload.type') == 'application/pdf') {
						$name = $this->Fatura->id . '_' . md5(time()) . '.pdf';
						$path = WWW_ROOT . 'arquivos' . DS . 'faturas' . DS;
						if (move_uploaded_file($this->request->data('upload.tmp_name'), $path . $name)) {
							$this->request->data('Fatura.arquivo', $name);
							if (!empty($registro['Fatura']['arquivo'])) {
								unlink($path . $registro['Fatura']['arquivo']);
							}
						} else {
							$success = false;
						}
					} else {
						$success = false;
					}
				}
				if (isset($this->request->data['upload'])) {
					unset($this->request->data['upload']);
				}
				
				if ($success) {
					if ($this->Fatura->saveAll($this->Fatura->prepareToSave($this->request->data))) {
						$this->Session->setFlash(__('Registro salvo com sucesso.'), 'cms_msg', ['class' => 'success'], 'cms');
						return $this->redirect(['cms' => true, 'action' => 'edit', $this->Fatura->id]);
					}
				} else {
					$this->request->data('Fatura.arquivo', $registro['Fatura']['arquivo']);
				}
				$this->Session->setFlash(__('Ocorreu algum erro.'), 'cms_msg', ['class' => 'danger'], 'cms');
			} catch(Exception $e) {
				$this->Session->setFlash(__('Ocorreu algum erro.'), 'cms_msg', ['class' => 'danger'], 'cms');
			}
		} else {
			$this->request->data = $registro;
			if ($this->request->data('Fatura.data')) {
				$this->request->data('Fatura.data', Util::inverte($this->request->data('Fatura.data'), '-', '/'));
			}
		}
		if (isset($this->request->data['upload'])) {
			unset($this->request->data['upload']);
		}

		$empresas = $this->Fatura->Empresa->find('list');
		$this->set(compact('empresas'));
	}

/**
 * cms_remove method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cms_remove($id = null) {
		$this->Fatura->id = $id;
		if (!$this->Fatura->exists()) {
			throw new NotFoundException(__('Registro não encontrado.'));
		}
		if ($this->Fatura->delete()) {
			$this->Session->setFlash(__('Registro removido com sucesso.'), 'cms_msg', ['class' => 'success'], 'cms');
		} else {
			$this->Session->setFlash(__('Ocorreu algum erro.'), 'cms_msg', ['class' => 'danger'], 'cms');
		}
		return $this->redirect(['action' => 'index', 'cms' => true]);
	}

/**
 * cms_activate method
 *
 * @throws NotFoundException
 * @param string $id
 * @param integer $true
 * @return void
 */
	public function cms_activate($id = null, $true = 1) {
		$this->Fatura->id = $id;
		if (!$this->Fatura->exists()) {
			throw new NotFoundException(__('Registro não encontrado.')); 
		}
		if ($this->Fatura->saveField('ativo', (int) $true)) {
			$this->Session->setFlash(__('Registro atualizado com sucesso.'), 'cms_msg', ['class' => 'success'], 'cms');
		} else {
			$this->Session->setFlash(__('Ocorreu algum erro.'), 'cms_msg', ['class' => 'danger'], 'cms');
		}
		return $this->redirect($this->referer());
	}
}