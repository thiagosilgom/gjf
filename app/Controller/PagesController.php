<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = [];

	public function cms_index() {
		$this->set('module_config', [
			'title'       => 'CMS',
			'description' => 'Content Management System'
		]);
	}

	public function home() {
		// $this->loadModel('Pagina');
		// $pagina = $this->Pagina->read(null, 1);
		// $this->set(compact('pagina'));

		return $this->redirect(['action' => 'login']);
	}

	public function login() {
		if ($this->request->is(['post','put'])) {
			$this->loadModel('Cadastro');
			$cadastro = $this->Cadastro->find('first', [
				'conditions' => [
					'Cadastro.email' => $this->request->data('Login.email'),
					'Cadastro.senha' => md5($this->request->data('Login.senha')),
					'Cadastro.ativo' => true
				],
				'recursive' => -1
			]);
			if (!empty($cadastro)) {
				$this->Session->write('CadastroID', $cadastro['Cadastro']['id']);
				return $this->redirect(['action' => 'area_restrita']);
			} else {
				$this->Session->setFlash(__('E-mail e senha não conferem.'), 'msg', ['class' => 'danger'], 'sistema');
			}
		}
		if ($this->request->data('Login.senha')) {
			unset($this->request->data['Login']['senha']);
		}
	}

	public function logout() {
		$this->layout = false;
		$this->autoRender = false;

		$this->Session->delete('CadastroID');
		return $this->redirect(['action' => 'login']);
	}

	public function _verificar() {
		if (!$this->Session->check('CadastroID')) {
			return $this->redirect(['action' => 'login']);
		}

		$this->loadModel('Cadastro');
		$cadastro = $this->Cadastro->find('first', [
			'conditions' => [
				'Cadastro.id' => $this->Session->read('CadastroID'),
				'Cadastro.ativo' => true
			],
			'recursive' => -1
		]);

		if (empty($cadastro)) {
			return $this->redirect(['action' => 'login']);
		}

		$this->set(compact('cadastro'));

		return $cadastro;
	}

	public function area_restrita() {
		$this->layout = false;
		$this->autoRender = false;

		return $this->redirect(['action' => 'meus_dados_cadastrais']);
	}

	public function meus_dados_cadastrais() {
		$cadastro = $this->_verificar();

		$this->loadModel('Cadastro');
		if ($this->request->is(['post','put'])) {
			$this->loadModel('Cadastro');
			$this->request->data('Cadastro.id', $cadastro['Cadastro']['id']);
			if ($this->request->data('Cadastro.empresa_id')) {
				unset($this->request->data['Cadastro']['empresa_id']);
			}
			if ($this->request->data('Cadastro.senha')) {
				unset($this->request->data['Cadastro']['senha']);
			}
			$error = false;
			if ($this->request->data('Cadastro.email')) {
				$verify = $this->Cadastro->find('first', [
					'conditions' => [
						'Cadastro.id <>' => $cadastro['Cadastro']['id'],
						'Cadastro.email' => $this->request->data('Cadastro.email')
					],
					'recursive' => -1
				]);
				if (!empty($verify)) {
					$error = true;
					$this->Session->setFlash(__('O e-mail já está cadastrado com outro usuário.'), 'msg', ['class' => 'danger'], 'sistema');
				}
			} else {
				$error = true;
				$this->Session->setFlash(__('O e-mail deve ser preenchido.'), 'msg', ['class' => 'danger'], 'sistema');
			}
			if ($this->request->data('Cadastro.nova_senha')) {
				if ($this->request->data('Cadastro.nova_senha') == $this->request->data('Cadastro.confirmacao_senha')) {
					$this->request->data('Cadastro.senha', md5($this->request->data('Cadastro.nova_senha')));
				} else {
					$error = true;
					$this->Session->setFlash(__('A nova senha e a sua confirmação estão diferentes.'), 'msg', ['class' => 'danger'], 'sistema');
				}
			}
			if (!$error) {
				if ($this->Cadastro->save($this->request->data('Cadastro'))) {
					$this->Session->setFlash(__('Cadastro salvo com sucesso.'), 'msg', ['class' => 'success'], 'sistema');
					return $this->redirect(['action' => 'meus_dados_cadastrais']);
				}
				$this->Session->setFlash(__('Ocorreu algum erro.'), 'msg', ['class' => 'danger'], 'sistema');
			}
		}
		if (!isset($this->request->data['Cadastro'])) {
			$this->request->data('Cadastro', $cadastro['Cadastro']);
		}
		if ($this->request->data('Cadastro.nova_senha')) {
			unset($this->request->data['Cadastro']['nova_senha']);
		}
		if ($this->request->data('Cadastro.confirmacao_senha')) {
			unset($this->request->data['Cadastro']['confirmacao_senha']);
		}
	}

	public function financeiro() {
		$cadastro = $this->_verificar();

		$this->loadModel('Boleto');
		$boletos = $this->Boleto->find('all', [
			'conditions' => [
				'Boleto.empresa_id' => $cadastro['Cadastro']['empresa_id'],
				'Boleto.ativo' => true
			],
			'order' => ['Boleto.data' => 'desc', 'Boleto.id' => 'desc'],
			'recursive' => -1
		]);
		$this->set(compact('boletos'));
	}

	public function financeiro_zip() {
		$this->layout = false;
		$this->autoRender = false;

		$cadastro = $this->_verificar();

		$this->loadModel('Boleto');
		$boletos = $this->Boleto->find('all', [
			'conditions' => [
				'Boleto.empresa_id' => $cadastro['Cadastro']['empresa_id'],
				'Boleto.ativo' => true
			],
			'order' => ['Boleto.data' => 'desc', 'Boleto.id' => 'desc'],
			'recursive' => -1
		]);

		$name = $cadastro['Cadastro']['id'] . '_' . md5(rand(1,1000000)) . '_' . md5(time()) . '.zip';
		$path = WWW_ROOT . 'arquivos' . DS . 'boletos' . DS;
		
		$zip = new ZipArchive;
		if ($zip->open($path . $name, ZipArchive::CREATE) === TRUE) {
			foreach ($boletos as $boleto) {
				$zip->addFile($path . $boleto['Boleto']['arquivo'], $boleto['Boleto']['id'] . '_' . Util::slug($boleto['Boleto']['titulo']) . '.pdf');
			}
			$zip->close();
		}

		header("Content-type: application/zip"); 
		header("Content-Disposition: attachment; filename=$name");
		header("Content-length: " . filesize($path . $name));
		header("Pragma: no-cache");
		header("Expires: 0"); 
		readfile($path . $name);
		unlink($path . $name);
	}

	public function faturas() {
		$cadastro = $this->_verificar();

		$this->loadModel('Fatura');

		$mes = '';
		$split = [];
		if (isset($this->request->query['mes']) && !empty($this->request->query['mes'])) {
			$split = explode('-', $this->request->query['mes']);
			if (count($split) == 2) {
				$mes = $this->request->query['mes'];
			}
		}

		if (empty($mes)) {
			$meses = $this->Fatura->find('list', [
				'fields' => ['mes'],
				'conditions' => [
					'Fatura.empresa_id' => $cadastro['Cadastro']['empresa_id'],
					'Fatura.ativo' => true
				],
				'order' => ['Fatura.mes' => 'desc', 'Fatura.id' => 'desc'],
				'group' => ['Fatura.mes'],
				'recursive' => -1
			]);
			$this->set(compact('meses'));
		} else {
			$faturas = $this->Fatura->find('all', [
				'conditions' => [
					'Fatura.empresa_id' => $cadastro['Cadastro']['empresa_id'],
					'Fatura.mes' => $mes,
					'Fatura.ativo' => true
				],
				'order' => ['Fatura.data' => 'desc', 'Fatura.id' => 'desc'],
				'recursive' => -1
			]);
			$this->set(compact('faturas'));
		}

		$this->set(compact('mes','split'));
	}

	public function faturas_zip() {
		$this->layout = false;
		$this->autoRender = false;

		$cadastro = $this->_verificar();

		$mes = '';
		if (isset($this->request->query['mes']) && !empty($this->request->query['mes'])) {
			$split = explode('-', $this->request->query['mes']);
			if (count($split) == 2) {
				$mes = $this->request->query['mes'];
			}
		}

		$this->loadModel('Fatura');
		$faturas = $this->Fatura->find('all', [
			'conditions' => [
				'Fatura.empresa_id' => $cadastro['Cadastro']['empresa_id'],
				'Fatura.mes' => $mes,
				'Fatura.ativo' => true
			],
			'order' => ['Fatura.data' => 'desc', 'Fatura.id' => 'desc'],
			'recursive' => -1
		]);

		$name = $cadastro['Cadastro']['id'] . '_' . md5(rand(1,1000000)) . '_' . md5(time()) . '.zip';
		$path = WWW_ROOT . 'arquivos' . DS . 'faturas' . DS;
		
		$zip = new ZipArchive;
		if ($zip->open($path . $name, ZipArchive::CREATE) === TRUE) {
			foreach ($faturas as $fatura) {
				$zip->addFile($path . $fatura['Fatura']['arquivo'], $fatura['Fatura']['id'] . '_' . Util::slug($fatura['Fatura']['titulo']) . '.pdf');
			}
			$zip->close();
		}

		header("Content-type: application/zip"); 
		header("Content-Disposition: attachment; filename=$name");
		header("Content-length: " . filesize($path . $name));
		header("Pragma: no-cache");
		header("Expires: 0"); 
		readfile($path . $name);
		unlink($path . $name);
	}

	public function solicitacoes($id = null) {
		$cadastro = $this->_verificar();

		$this->loadModel('Solicitacao');

		if ($id === null) {
			if ($this->request->is(['post','put'])) {
				$now = date('Y-m-d H:i:s');
				$this->request->data('Solicitacao.created', $now);
				$this->request->data('Solicitacao.modified', $now);
				$this->request->data('Solicitacao.empresa_id', $cadastro['Cadastro']['empresa_id']);
				$this->request->data('Solicitacao.cadastro_id', $cadastro['Cadastro']['id']);
				$this->request->data('Solicitacao.finalizado', 0);
				$this->request->data('Solicitacao.data_ultima_mensagem', $now);
				if ($this->Solicitacao->save($this->request->data('Solicitacao'))) {
					$this->Session->setFlash(__('Solicitação salva com sucesso.'), 'msg', ['class' => 'success'], 'sistema');
					return $this->redirect(['action' => 'solicitacoes']);
				}
				$this->Session->setFlash(__('Ocorreu algum erro.'), 'msg', ['class' => 'danger'], 'sistema');
			}

			$solicitacoes = $this->Solicitacao->find('all', [
				'conditions' => ['Solicitacao.empresa_id' => $cadastro['Cadastro']['empresa_id']],
				'order' => ['Solicitacao.id' => 'desc']
			]);
			$this->set(compact('solicitacoes'));
		} else {
			$this->Solicitacao->unbindModel(['belongsTo' => ['Empresa']]);
			$this->Solicitacao->Cadastro->unbindModel(['belongsTo' => ['Empresa'], 'hasMany' => ['Solicitacao','Resposta']]);
			$this->Solicitacao->Resposta->unbindModel(['belongsTo' => ['Solicitacao']]);
			$solicitacao = $this->Solicitacao->find('first', [
				'conditions' => [
					'Solicitacao.id' => $id,
					'Solicitacao.empresa_id' => $cadastro['Cadastro']['empresa_id']
				],
				'recursive' => 2
			]);
			
			if (empty($solicitacao)) {
				throw new NotFoundException(__('Registro não encontrado.'));
			}

			if ($this->request->is(['post', 'put'])) {
				$now = date('Y-m-d H:i:s');
				$this->request->data('Resposta.created', $now);
				$this->request->data('Resposta.modified', $now);
				$this->request->data('Resposta.solicitacao_id', $id);
				$this->request->data('Resposta.usuario_id', null);
				$this->request->data('Resposta.cadastro_id', $cadastro['Cadastro']['id']);
				$this->request->data('Resposta.cms', 0);
				if ($this->Solicitacao->Resposta->save($this->request->data('Resposta'))) {
					if ($this->Solicitacao->save(['id' => $id, 'data_ultima_mensagem' => $now])) {
						$this->Session->setFlash(__('Registro atualizado com sucesso.'), 'msg', ['class' => 'success'], 'sistema');
						return $this->redirect(['action' => 'solicitacoes', 'id' => $id]);
					}
				}
				$this->Session->setFlash(__('Ocorreu algum erro.'), 'msg', ['class' => 'danger'], 'sistema');
			}

			$this->set(compact('solicitacao'));
		}

		$this->set(compact('id'));
	}
}
