<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?= (isset($module_config['title']) ? $module_config['title'] : ''); ?>
    <?php if (isset($module_config['description']) && !empty($module_config['description'])) : ?>
    	<small><?= $module_config['description']; ?></small>
    <?php endif; ?>
  </h1>
</section>