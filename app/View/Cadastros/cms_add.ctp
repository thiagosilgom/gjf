<?= $this->element('cms_module_header') ?>

<!-- Main content -->
<section class="content">

	<div class="box">
		<div class="box-header with-border">
			<i class="fa fa-plus"></i>
			<h3 class="box-title">Adicionar Usuário</h3>
		</div><!-- /.box-header -->

		<?= $this->Form->create('Cadastro', ['inputDefaults' => ['div' => ['class' => 'col-xs-12 col-md-6 form-group'], 'class' => 'form-control']]) ?>
			<div class="box-body">
				<div class="row">
					<div class="col-xs-12">
						<?= $this->Html->link('<i class="fa fa-list"></i> Listagem', ['action' => 'index', 'cms' => true], ['escape' => false, 'class' => 'btn btn-app']) ?>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<?= $this->Session->flash('cms') ?>
					</div>
				</div>

				<div class="margin">
					<div class="row">
						<?= $this->Form->input('empresa_id', ['label' => 'Empresa', 'empty' => 'Selecione']) ?>
					</div>
					<div class="row">
						<?= $this->Form->input('nome', ['label' => 'Nome']) ?>
					</div>
					<div class="row">
						<?= $this->Form->input('email', ['label' => 'E-mail', 'type' => 'email']) ?>
					</div>
					<div class="row">
						<?= $this->Form->input('telefone', ['label' => 'Telefone']) ?>
					</div>
					<div class="row">
						<div class="col-xs-12 form-group">
							<div class="checkbox">
								<label>
									<?= $this->Form->input('ativo', ['label' => false, 'div' => false, 'class' => false, 'type' => 'checkbox']) ?>
									Ativo
								</label>
							</div>
						</div>
					</div>
				</div>
			</div><!-- /.box-body -->

			<div class="box-footer">
				<?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-lg btn-primary pull-right']) ?>
			</div>
		<?= $this->Form->end() ?>
	</div><!-- /.box -->

</section><!-- /.content -->