<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>CMS</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

		<?= $this->Html->meta('favicon.png', '/favicon.png', ['type' => 'icon']) ?>

		<!-- Bootstrap 3.3.5 -->
		<link rel="stylesheet" href="<?= $this->webroot ?>bootstrap/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
		<link rel="stylesheet" href="https://fastcdn.org/Font-Awesome/4.4.0/css/font-awesome.min.css">
		<!-- Ionicons -->
		<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
		<!-- daterange picker -->
		<link rel="stylesheet" href="<?= $this->webroot ?>plugins/daterangepicker/daterangepicker-bs3.css">
		<!-- Bootstrap time Picker -->
		<link rel="stylesheet" href="<?= $this->webroot ?>plugins/timepicker/bootstrap-timepicker.min.css">
		<!-- Select2 -->
		<link rel="stylesheet" href="<?= $this->webroot ?>plugins/select2/select2.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="<?= $this->webroot ?>dist/css/AdminLTE.min.css">
		<!-- AdminLTE Skins. Choose a skin from the css/skins
				 folder instead of downloading all of them to reduce the load. -->
		<link rel="stylesheet" href="<?= $this->webroot ?>dist/css/skins/_all-skins.min.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
				<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<?= $this->Html->css('cms/style') ?>

		<?= $this->fetch('css') ?>
	</head>
	<body class="hold-transition skin-blue sidebar-mini <?= $this->Session->check('cms_sidebar_collapse') && $this->Session->read('cms_sidebar_collapse') ? 'sidebar-collapse' : '' ?>">
		<!-- Site wrapper -->
		<div class="wrapper">

			<header class="main-header">
				<!-- Logo -->
				<a href="<?= $this->webroot ?>" target="_blank" class="logo">
					<!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><?= $this->Html->image('/img/site/logo-white-min.png') ?></span>
					<!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><?= $this->Html->image('/img/site/logo-white.png', ['style' => 'width:auto; height:40px;']) ?></span>
				</a>
				<!-- Header Navbar: style can be found in header.less -->
				<nav class="navbar navbar-static-top" role="navigation">
					<!-- Sidebar toggle button-->
					<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<!-- User Account: style can be found in dropdown.less -->
							<li class="dropdown user user-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<?= ($the_user['Thumb']['file'] !== null ? $this->Html->image($this->Image->resizedUrl($the_user['Thumb']['file'], 160, 160), ['class' => 'user-image', 'alt' => 'Foto do Usuário']) : '<img src="'.$this->webroot.'dist/img/user-semfoto.png" class="user-image" alt="Foto do Usuário">') ?>
									<span class="hidden-xs"><?= $the_user['Usuario']['nome'] ?></span>
								</a>
								<ul class="dropdown-menu">
									<!-- User image -->
									<li class="user-header">
										<?= ($the_user['Thumb']['file'] !== null ? $this->Html->image($this->Image->resizedUrl($the_user['Thumb']['file'], 160, 160), ['class' => 'img-circle', 'alt' => 'Foto do Usuário']) : '<img src="'.$this->webroot.'dist/img/user-semfoto.png" class="img-circle" alt="Foto do Usuário">') ?>
										<p>
											<?= $the_user['Usuario']['nome'] ?>
											<small><?= $the_user['Usuario']['login'] ?></small>
										</p>
									</li>
									<!-- Menu Footer-->
									<li class="user-footer">
										<div class="pull-left">
											<?= $this->Html->link(
												__('Editar Dados'),
												['controller' => 'usuarios', 'action' => 'edit', 'cms' => true, $the_user['Usuario']['id']],
												['class' => 'btn btn-default btn-flat']
											) ?>
										</div>
										<div class="pull-right">
											<?= $this->Html->link(
												__('Sair'),
												['controller' => 'usuarios', 'action' => 'logout', 'cms' => false],
												['class' => 'btn btn-default btn-flat'],
												__('Deseja realmente sair do sistema?')
											) ?>
										</div>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</nav>
			</header>

			<!-- =============================================== -->

			<!-- Left side column. contains the sidebar -->
			<aside class="main-sidebar">
				<!-- sidebar: style can be found in sidebar.less -->
				<section class="sidebar">
					<!-- Sidebar user panel -->
					<div class="user-panel">
						<div class="pull-left image">
							<?= ($the_user['Thumb']['file'] !== null ? $this->Html->image($this->Image->resizedUrl($the_user['Thumb']['file'], 160, 160), ['class' => 'img-circle', 'alt' => 'Foto do Usuário']) : '<img src="'.$this->webroot.'dist/img/user-semfoto.png" class="img-circle" alt="Foto do Usuário">') ?>
						</div>
						<div class="pull-left info">
							<p><?= $the_user['Usuario']['nome'] ?></p>
							<?= $this->Html->link(
								'<i class="fa fa-user text-info"></i> Editar Dados',
								['controller' => 'usuarios', 'action' => 'edit', 'cms' => true, $the_user['Usuario']['id']],
								['escape' => false]
							) ?>
						</div>
					</div>

					<?= $this->element('cms_menu'); ?>
				</section>
				<!-- /.sidebar -->
			</aside>

			<!-- =============================================== -->

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<?= $this->fetch('content') ?>
			</div><!-- /.content-wrapper -->

			<footer class="main-footer">
				<?php /* <strong>&copy; <?= date('Y') ?> CakeCMS</strong> */ ?>
			</footer>
		</div><!-- ./wrapper -->

		<!-- jQuery 2.1.4 -->
		<script src="<?= $this->webroot ?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<!-- Bootstrap 3.3.5 -->
		<script src="<?= $this->webroot ?>bootstrap/js/bootstrap.min.js"></script>
		<!-- Select2 -->
		<script src="<?= $this->webroot ?>plugins/select2/select2.full.min.js"></script>
		<!-- InputMask -->
		<script src="<?= $this->webroot ?>plugins/input-mask/jquery.inputmask.js"></script>
		<script src="<?= $this->webroot ?>plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
		<script src="<?= $this->webroot ?>plugins/input-mask/jquery.inputmask.extensions.js"></script>
		<!-- date-range-picker -->
		<script src="<?= $this->webroot ?>plugins/updates/daterangepicker/moment.min.js"></script>
		<script src="<?= $this->webroot ?>plugins/updates/daterangepicker/daterangepicker.js"></script>
		<!-- bootstrap time picker -->
		<script src="<?= $this->webroot ?>plugins/timepicker/bootstrap-timepicker.min.js"></script>
		<!-- SlimScroll -->
		<script src="<?= $this->webroot ?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
		<!-- FastClick -->
		<script src="<?= $this->webroot ?>plugins/fastclick/fastclick.min.js"></script>
		<!-- AdminLTE App -->
		<script src="<?= $this->webroot ?>dist/js/app.min.js"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="<?= $this->webroot ?>dist/js/demo.js"></script>

		<script>
			$(document).ready(function(){
				$('.sidebar-toggle').click(function(){
					var collapse = $('body').hasClass('sidebar-collapse') ? 1 : 0;
					$.ajax({
						url: '<?= Router::url(['controller' => 'usuarios', 'action' => 'sidebar', 'cms' => true], true) ?>',
						data: {
							collapse: collapse
						},
						cache: false
					});
				});

				$('.cms-select-single').select2({
					placeholder: 'Selecione',
					allowClear: true
				});
				$('.cms-select-single-tag').select2({
					placeholder: 'Selecione ou crie uma nova',
					allowClear: true,
					tags: true
				});
				$('.cms-select-single-notnull').select2({
					placeholder: 'Selecione'
				});
				$('.cms-select-single-tag-notnull').select2({
					placeholder: 'Selecione',
					tags: true
				});
				$('.cms-select-multiple').select2({
					placeholder: 'Selecione'
				});
				$('.cms-select-multiple-tag').select2({
					placeholder: 'Selecione ou crie uma nova',
					tags: true
				});

				$('.input-group-addon').click(function(){
					$(this).parent().find('input').focus();
				});

				$('.cms-datepicker').inputmask('dd/mm/yyyy', {
					placeholder: 'dd/mm/aaaa'
				}).daterangepicker({
					singleDatePicker: true,
					showDropdowns: true,
					locale: {
						format: 'DD/MM/YYYY',
						daysOfWeek: ['D','S','T','Q','Q','S','S'],
						monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro']
					}
				});

				$('.cms-timepicker').inputmask('hh:mm', {
					placeholder: 'hh:mm'
				}).timepicker({
					showInputs: false,
					showMeridian: false
				});
			});
		</script>

		<?= $this->fetch('script') ?>
	</body>
</html>
