<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
	<title><?= $title_for_layout ?></title>
</head>
<body>
	<table style="display:block;border:1px solid #eee; margin:0 auto" border="0" cellpadding="0" cellspacing="0" width="672">
		<tr>
			<td style="background:#0089A5; text-align:center; padding:30px" width="612">
				<?php if (!empty($logo)) : ?>
					<?= $this->Html->link($this->Html->image($logo, ['fullBase' => true]), ['controller' => 'pages', 'action' => 'home', 'full_base' => true], ['escape' => false]) ?>
				<?php endif; ?>
				
				<?php if (!empty($title)) : ?>
					<h1 style="font-family:'Open Sans',sans-serif,'Helvetica Neue',Helvetica,Arial;font-size:25px;line-height:120%;color:#fff;text-align:center; font-weight:400; margin:0"><?= $title ?></h1>
				<?php endif; ?>

				<?php if (!empty($subtitle)) : ?>
					<p style="font-family:'Open Sans',sans-serif,'Helvetica Neue',Helvetica,Arial;font-size:16px;line-height:120%;color:#E2E2E2;text-align:center; font-weight:400; margin:0"><?= $subtitle ?></p>
				<?php endif; ?>
			</td>
		</tr>

		<?= $this->fetch('content') ?>

		<?php if ($showLink) : ?>
			<tr>
				<td style="padding:10px 30px; text-align:center" width="612">
					<?= $this->Html->link($link['label'], $link['url'], array('style' => 'background: #0089A5;border-radius: 3px;display: inline-block;color: #fff;font-family:\'Open Sans\',sans-serif,\'Helvetica Neue\',Helvetica,Arial;font-size: 16px;text-decoration: none;padding: 12px 10px 0px 10px;margin: 0 auto;width: 170px;height: 28px;')) ?>
				</td>
			</tr>
		<?php endif; ?>

		<tr>
			<td width="612">
				<p style="font-family:'Open Sans',sans-serif,'Helvetica Neue',Helvetica,Arial;font-size:13px;line-height:120%;color:#c9c9c9; font-weight:400; margin:0; text-align:center; border-top:1px solid #eee; padding:30px 0; background-color:#666666;"><?= $footer ?></p>
			</td>
		</tr>
	</table>
</body>
</html>