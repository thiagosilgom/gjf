<?= $this->element('header') ?>

<div class="container-fluid">
    <div class="row">
        <?= $this->element('sidebar', ['active' => 'faturas']) ?>

        <div class="col-lg-9 col-md-12 h100">
			<div class="mt160"></div>
            <div class="content-conteudo">
				<?php if (empty($mes)) : ?>
					<h1>Faturas</h1>
					<div class="mt50"></div>
					<?php $count = count($meses); ?>
					<?php $i = 0; ?>
					<?php foreach ($meses as $mes) : ?>
						<?php $split = explode('-', $mes); ?>
						<?= $this->Html->link('Faturas de ' . Util::mes($split[1]) . ' de ' . $split[0], ['action' => 'faturas', '?' => ['mes' => $mes]], ['class' => 'item-download']) ?>
						<?php if ($i < $count - 1) : ?>
							<div class="mt20"></div>
						<?php endif; ?>
						<?php $i++; ?>
					<?php endforeach; ?>
					<div class="mt50"></div>
					<?= $this->Html->link('preciso de faturas anteriores', ['action' => 'solicitacoes'], ['class' => 'botao-padrao']) ?>
				<?php else : ?>
					<h1>Faturas de <?= Util::mes($split[1]) ?> de <?= $split[0] ?></h1>
					<div class="mt50"></div>
					<?php $count = count($faturas); ?>
					<?php foreach ($faturas as $k => $fatura) : ?>
						<?php if (!empty($fatura['Fatura']['arquivo'])) : ?>
							<a href="<?= $this->webroot ?>arquivos/faturas/<?= $fatura['Fatura']['arquivo'] ?>" target="_blank" class="item-download"><?= $fatura['Fatura']['titulo'] ?> <img src="<?= $this->webroot ?>img/site/down.png" /></a>
							<?php if ($k < $count - 1) : ?>
								<div class="mt30"></div>
							<?php endif; ?>
						<?php endif; ?>
					<?php endforeach; ?>
					<div class="mt50"></div>
					<?php if (!empty($faturas)) : ?>
						<?= $this->Html->link('baixar todas', ['action' => 'faturas_zip', '?' => ['mes' => $mes]], ['class' => 'botao-padrao', 'target' => '_blank']) ?>
					<?php endif; ?>
				<?php endif; ?>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        function h100() {
            $('.h100').css('height', window.innerHeight);
        }h100();

        $(window).load(h100).resize(h100);
    })
</script>