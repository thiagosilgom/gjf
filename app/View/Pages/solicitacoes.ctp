<?= $this->element('header') ?>

<div class="container-fluid">
    <div class="row">
        <?= $this->element('sidebar', ['active' => 'solicitacoes']) ?>

        <div class="col-lg-9 col-md-12 h100">
			<div class="mt160"></div>
            <div class="content-conteudo">
				<?php if ($id === null) : ?>
					<h1>Solicitações</h1>
					<div class="mt50"></div>
					<?= $this->Session->flash('sistema') ?>
					<?php $count = count($solicitacoes); ?>
					<?php foreach ($solicitacoes as $k => $solicitacao) : ?>
						<?= $this->Html->link(Util::codigo($solicitacao['Solicitacao']['id']) . ' - ' . $solicitacao['Solicitacao']['titulo'], ['action' => 'solicitacoes', 'id' => $solicitacao['Solicitacao']['id']], ['class' => 'item-download']) ?>
						<?php if ($k < $count - 1) : ?>
							<div class="mt20"></div>
						<?php endif; ?>
					<?php endforeach; ?>
					<div class="mt100"></div>
					<h2>Nova solicitação</h2>
					<div class="mt50"></div>
					<?= $this->Form->create('Solicitacao', ['inputDefaults' => ['div' => false, 'label' => false]]) ?>
						<?= $this->Form->input('titulo', ['type' => 'text', 'placeholder' => 'Título da solicitação']) ?>
						<div class="mt20"></div>
						<?= $this->Form->input('mensagem', ['type' => 'textarea', 'placeholder' => 'Descrição do que você precisa, ex.: Preciso dos boletos para o pagamento dos meses em atraso.', 'cols' => 30]) ?>
						<div class="mt20"></div>
						<?= $this->Form->input('assunto', ['type' => 'text', 'placeholder' => 'Assunto da solicitação']) ?>
						<div class="mt20"></div>
						<?= $this->Form->input('criar nova', ['type' => 'submit']) ?>
					<?= $this->Form->end() ?>
					<div class="mt100"></div>
				<?php else : ?>
					<?= $this->Session->flash('sistema') ?>
					<div class="comentario">
						<h1><?= Util::codigo($solicitacao['Solicitacao']['id']) ?> - <?= $solicitacao['Solicitacao']['titulo'] ?></h1>
						<div class="mt30"></div>
						<div class="dados-solicitacoes">Data da solicitação: <?= (!empty($solicitacao['Solicitacao']['created']) ? Util::data($solicitacao['Solicitacao']['created']) : '') ?> - Usuário(a) <?= (isset($cadastro['Cadastro']['nome']) ? $cadastro['Cadastro']['nome'] : '') ?></div>
						<div class="mt10"></div>
						<p><?= nl2br($solicitacao['Solicitacao']['mensagem']) ?></p>
						<div class="mt30"></div>
						<div class="line"></div>
					</div>
					<?php foreach ($solicitacao['Resposta'] as $resposta) : ?>
						<div class="mt30"></div>
						<div class="comentario <?= ($resposta['cms'] ? 'text-right' : '') ?>">
							<div class="dados-solicitacoes">Data da resposta: <?= (!empty($resposta['created']) ? Util::data($resposta['created']) : '') ?> - Usuário(a) <?= (isset($resposta[$resposta['cms'] ? 'Usuario' : 'Cadastro']['nome']) ? $resposta[$resposta['cms'] ? 'Usuario' : 'Cadastro']['nome'] : '') ?></div>
							<div class="mt10"></div>
							<p><?= nl2br($resposta['mensagem']) ?></p>
							<div class="mt30"></div>
							<div class="line"></div>
						</div>
					<?php endforeach; ?>
					<div class="mt30"></div>
					<a class="botao-padrao" data-toggle="collapse" href="#solicitacao_responder" role="button" aria-expanded="false" aria-controls="solicitacao_responder">responder</a>
					<div class="collapse" id="solicitacao_responder">
						<div class="mt30"></div>
						<div class="pd-30">
							<label for="descricao">Adicionar resposta</label>
							<div class="mt5"></div>
							<?= $this->Form->create('Resposta', ['url' => ['controller' => 'pages', 'action' => 'solicitacoes', 'id' => $solicitacao['Solicitacao']['id']], 'inputDefaults' => ['div' => false, 'label' => false]]) ?>
								<?= $this->Form->input('mensagem', ['type' => 'textarea', 'placeholder' => 'Descrição do que você precisa, ex.: Preciso dos boletos para o pagamento dos meses em atraso.', 'cols' => 30]) ?>
								<div class="mt20"></div>
								<div class="text-right">
									<?= $this->Form->input('enviar', ['type' => 'submit']) ?>
								</div>
							<?= $this->Form->end() ?>
						</div>
					</div>
				<?php endif; ?>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        function h100() {
            $('.h100').css('height', window.innerHeight);
        }h100();

        $(window).load(h100).resize(h100);
    })
</script>