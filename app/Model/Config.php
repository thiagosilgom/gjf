<?php
/**
 * Config Model
 *
 */
class Config extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nome';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = [
		'titulo' => [
			'notBlank' => [
				'rule'    => 'notBlank',
				'message' => 'O título não pode ser vazio.'
			]
		]
	];
}
