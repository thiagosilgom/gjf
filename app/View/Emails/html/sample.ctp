<?php foreach ($fields as $label => $value) : ?>
	<tr>
		<td style="padding:10px 30px" width="612">
			<p style="font-family:'Open Sans',sans-serif,'Helvetica Neue',Helvetica,Arial;font-size:16px;line-height:120%;color:#444; font-weight:400; margin:0 0 5px"><?= $label ?>:</p>
			<p style="font-family:'Open Sans',sans-serif,'Helvetica Neue',Helvetica,Arial;font-size:16px;line-height:120%;color:#999; font-weight:400; margin:0"><?= nl2br($value) ?></p>
		</td>
	</tr>
<?php endforeach; ?>