<?php
	$menu = [
		[
			'controller' => 'usuarios',
			'icon'       => 'fa fa-user',
			'title'      => 'Usuários do CMS'
		],
		[
			'controller' => 'configs',
			'icon'       => 'fa fa-gear',
			'title'      => 'Configurações'
		],
		// [
		// 	'controller' => 'paginas',
		// 	'icon'       => 'fa fa-newspaper-o',
		// 	'title'      => 'Páginas'
		// ],
		[
			'controller' => 'empresas',
			'icon'       => 'fa fa-institution',
			'title'      => 'Empresas'
		],
		[
			'controller' => 'cadastros',
			'icon'       => 'fa fa-users',
			'title'      => 'Usuários de Empresas'
		],
		[
			'controller' => 'boletos',
			'icon'       => 'fa fa-usd',
			'title'      => 'Financeiro'
		],
		[
			'controller' => 'faturas',
			'icon'       => 'fa fa-file-text',
			'title'      => 'Faturas'
		],
		[
			'controller' => 'solicitacaos',
			'icon'       => 'fa fa-commenting',
			'title'      => 'Solicitações'
		],
	];
	$the_controller = $this->request->params['controller'];
	foreach ($menu as &$item) :
		if (!isset($item['children'])) :
			$item['active'] = ($item['controller'] == $the_controller);
		else :
			$item['active'] = false;
			foreach ($item['children'] as &$child) :
				if ($child['active'] = ($child['controller'] == $the_controller)) :
					$item['active'] = true;
				endif;
			endforeach;
			unset($child);
		endif;
	endforeach;
	unset($item);
?>
<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu">
	<li class="header">MENU DE NAVEGAÇÃO</li>
	<?php foreach ($menu as $item) : ?>
		<?php if (!isset($item['children'])) : ?>
			<li class="<?= ($item['active'] ? 'active' : '') ?>">
				<?=
					$this->Html->link(
						sprintf(
								'<i class="%s"></i> <span>%s</span>',
							(isset($item['icon']) ? $item['icon'] : 'fa fa-circle-o'),
							$item['title']
						),
						['controller' => $item['controller'], 'action' => 'index', 'cms' => true, 'plugin' => null],
						['escape' => false]
					)
				?>
			</li>
		<?php else : ?>
			<li class="treeview <?= ($item['active'] ? 'active' : '') ?>">
				<?=
					$this->Html->link(
						sprintf(
								'<i class="%s"></i> <span>%s</span><i class="fa fa-angle-left pull-right"></i>',
							(isset($item['icon']) ? $item['icon'] : 'fa fa-circle-o'),
							$item['title']
						),
						'#',
						['escape' => false]
					)
				?>
				<ul class="treeview-menu">
					<?php foreach ($item['children'] as $child) : ?>
						<li class="<?= ($child['active'] ? 'active' : '') ?>">
							<?=
								$this->Html->link(
									sprintf(
											'<i class="%s"></i> <span>%s</span>',
										(isset($child['icon']) ? $child['icon'] : 'fa fa-circle-o'),
										$child['title']
									),
									['controller' => $child['controller'], 'action' => 'index', 'cms' => true, 'plugin' => null],
									['escape' => false]
								)
							?>
						</li>
					<?php endforeach; ?>
				</ul>
			</li>
		<?php endif; ?>
	<?php endforeach; ?>
</ul>