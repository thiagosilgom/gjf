<?php if ($this->Paginator->counter(['format' => '{:pages}']) > 1) : ?>
	<div class="margin">
		<div class="row">
			<div class="col-xs-12">
				<ul class="pagination pagination-sm no-margin pull-right">
					<?php if ($this->Paginator->counter(['format' => '{:page}']) > 1) : ?>
						<li>
							<?= $this->Paginator->prev('«', ['tag'=>false, 'class'=>false, 'rel'=>false]) ?>
						</li>
					<?php endif; ?>

					<?= $this->Paginator->numbers([
						'tag'          => 'li',
						'currentTag'   => 'span',
						'currentClass' => 'active',
						'separator'    => ''
					]) ?>

					<?php if ($this->Paginator->counter(['format' => '{:page}']) < $this->Paginator->counter(['format' => '{:pages}'])) : ?>
						<li class="next">
							<?= $this->Paginator->next('»', ['tag'=>false, 'class'=>false, 'rel'=>false]) ?>
						</li>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	</div>
<?php endif; ?>