<?= $this->element('cms_module_header') ?>

<!-- Main content -->
<section class="content">

	<div class="box">
		<div class="box-header with-border">
			<i class="fa fa-list"></i>
			<h3 class="box-title">Listagem de Faturas</h3>
		</div><!-- /.box-header -->
		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<?= $this->Html->link('<i class="fa fa-plus"></i> Adicionar', ['action' => 'add', 'cms' => true], ['escape' => false, 'class' => 'btn btn-app']) ?>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<?= $this->Form->create('Fatura', ['inputDefaults' => ['div' => false, 'label' => false], 'type' => 'get', 'url' => ['action' => 'index', 'cms' => true]]) ?>
					<div class="row" style="margin-right:10px">
							<div class="col-xs-12 form-group margin">
								<?= $this->Form->input('empresa_id', ['empty' => __('Empresa'), 'class' => 'form-control', 'required' => false]) ?>
							</div>
						</div>
						<div class="row" style="margin-right:10px">
							<div class="col-xs-12 form-group margin">
								<?= $this->Form->input('busca', ['placeholder' => __('Digite o título...'), 'class' => 'form-control']) ?>
							</div>
						</div>
						<div class="row" style="margin-right:10px">
							<div class="col-xs-12 form-group margin">
								<?= $this->Form->button(__('Buscar'), ['class' => 'btn btn-info btn-flat pull-right']) ?>
							</div>
						</div>
					<?= $this->Form->end() ?>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<?= $this->Session->flash('cms') ?>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<?php if (empty($the_list)) : ?>
						<?= $this->element('cms_msg', ['message' => __('Nenhum registro encontrado.'), 'class' => 'warning', 'close' => false]) ?>
					<?php else : ?>
						<div class="margin">
							<div class="box-body table-responsive no-padding">
								<table class="table table-hover table-striped">
									<tbody>
										<tr>
											<th class="hidden-xs">ID</th>
											<th>Empresa</th>
											<th>Título</th>
											<th>Data</th>
											<th class="hidden-sm hidden-md hidden-lg">Ativo</th>
											<th class="hidden-xs">Ações</th>
										</tr>
										<?php foreach ($the_list as $item) : ?>
											<tr>
												<td class="hidden-xs"><?= $this->Html->link($item['Fatura']['id'], ['action' => 'edit', 'cms' => true, $item['Fatura']['id']], ['class' => 'text-black']) ?></td>
												<td><?= $this->Html->link($item['Empresa']['titulo'], ['action' => 'edit', 'cms' => true, $item['Fatura']['id']], ['class' => 'text-black']) ?></td>
												<td><?= $this->Html->link($item['Fatura']['titulo'], ['action' => 'edit', 'cms' => true, $item['Fatura']['id']], ['class' => 'text-black']) ?></td>
												<td><?= $this->Html->link((!empty($item['Fatura']['data']) ? Util::inverte($item['Fatura']['data'], '-', '/') : ''), ['action' => 'edit', 'cms' => true, $item['Fatura']['id']], ['class' => 'text-black']) ?></td>
												<td class="hidden-sm hidden-md hidden-lg"><?= $this->Html->link($item['Fatura']['ativo'] ? 'Sim' : 'Não', ['action' => 'edit', 'cms' => true, $item['Fatura']['id']], ['class' => $item['Fatura']['ativo'] ? 'text-green' : 'text-red']) ?></td>
												<td class="hidden-xs">
													<?=
														!$item['Fatura']['ativo']
														? $this->Html->link('<i class="fa fa-ban text-red"></i>', ['action' => 'activate', 'cms' => true, $item['Fatura']['id'], 1], ['escape' => false, 'class' => 'link-acao'])
														: $this->Html->link('<i class="fa fa-check text-green"></i>', ['action' => 'activate', 'cms' => true, $item['Fatura']['id'], 0], ['escape' => false, 'class' => 'link-acao'])
													?>
													<?= $this->Html->link('<i class="fa fa-edit text-black"></i>', ['action' => 'edit', 'cms' => true, $item['Fatura']['id']], ['escape' => false, 'class' => 'link-acao']) ?>
													<?= $this->Html->link('<i class="fa fa-remove text-black"></i>', ['action' => 'remove', 'cms' => true, $item['Fatura']['id']], ['escape' => false, 'class' => 'link-acao'], __('Deseja realmente remover o registro?')) ?>
												</td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>

			<?= $this->element('cms_paginacao') ?>
		</div><!-- /.box-body -->
	</div><!-- /.box -->

</section><!-- /.content -->