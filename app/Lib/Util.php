<?php
App::uses('Inflector', 'Utility');
App::uses('CakeEmail', 'Network/Email');

class Util
{
	public static function slug($value) {
		return strtolower(Inflector::slug($value, '-'));
	}

	public static function excerpt($text, $num_of_words = 20, $read_more = '[..]') {
		if (!is_numeric($num_of_words)) {
			$num_of_words = 15;
		}
		$text = preg_replace ('/<br(\s*)\/?>/', ' ', html_entity_decode($text, ENT_QUOTES, 'UTF-8')); 
		$parts = explode(' ', str_replace('&nbsp;', ' ', strip_tags($text)));
		if (count($parts) <= $num_of_words) {
			return implode(" ", $parts);
		}
		$j =0;
		foreach ($parts as $p) {
			if (!empty($p)) {
				$return[$j++] = $p;
			}
			if ($j >= $num_of_words) {
				break;
			}
		}
		return implode(" ", $return) . $read_more;
	}

	public static function inverte($data, $separator = '/', $return = '-') {
		$data = explode($separator, $data);
		return sprintf(
			'%s%s%s%s%s',
			$data[2],
			$return,
			$data[1],
			$return,
			$data[0]
		);
	}

	public static function isSqlDate($data) {
		return preg_match('/[0-9]{4}\-[0-9]{2}\-[0-9]{2}(\s[0-9]{2}\:[0-9]{2}\:-[0-9]{2})?/', $data);
	}

	public static function multiemail($emails, $separator = ',') {
		$result = [];
		if (!is_array($emails))
			$emails = explode($separator, $emails);
		foreach ($emails as $item) {
			$address = trim($item);
			if (!empty($address))
				$result[] = $address;
		}
		return $result;
	}

	public static function email($config, $configs = null) {
		$config += [
			'subject'     => 'Contato',
			'logo'        => (isset($configs['logo']) ? $configs['logo'] : ''),
			'title'       => (isset($configs['titulo']) ? $configs['titulo'] : ''),
			'subtitle'    => (isset($configs['subject']) ? $configs['subject'] : 'Contato'),
			'fields'      => [],
			'showLink'    => true,
			'footer'      => (isset($configs['endereco']) ? nl2br($configs['endereco']) : ''),
			'layout'      => 'sample',
			'template'    => 'sample',
			'to'          => (isset($configs['email']) ? $configs['email'] : ''),
			'attachments' => []
		];
		$link = (isset($configs['link']) ? $configs['link'] : []) + [
			'label' => 'Ir para o site',
			'url'   => ['controller' => 'pages', 'action' => 'home', 'full_base' => true]
		];
		$config['link'] = $link;

		$config['to'] = self::multiemail($config['to']);
		if (empty($config['to']))
			return -1;
		
		try {
			$email = new CakeEmail('smtp');
			$email->subject($config['subject'])
				  ->to($config['to'])
				  ->emailFormat('html')
				  ->viewVars($config)
				  ->template($config['template'], $config['layout']);
			if (!empty($config['attachments']))
				$email->attachments($config['attachments']);
			$email->send();
		} catch(Exception $e) {
			return 0;
		}
		return 1;
	}

	public static function escapeCell($cell) {
		return '"' . preg_replace('/"/', '""', utf8_decode($cell)) . '"';
	}

	public static function hash($random_string_length = 8) {
		$characters = 'ABCDE0123456789';

		$string = '';
		$max = strlen($characters) - 1;
		for ($i = 0; $i < $random_string_length; $i++) {
			$string .= $characters[mt_rand(0, $max)];
		}

		return $string;
	}

	public static function codigo($id) {
		return str_pad($id, 4, '0', STR_PAD_LEFT);
	}

	public static function mes($mes) {
		$meses = [1=>'Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'];
		return $meses[(int) $mes];
	}

	public static function data($data) {
		$explode = explode(' ', $data);
		$split = explode('-', $explode[0]);
		return $split[2] . ' de ' . self::mes($split[1]) . ' de ' . $split[0];
	}
}