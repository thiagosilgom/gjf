<?= $this->Form->create('Usuario', ['action' => 'login', 'inputDefaults' => ['label' => false, 'div' => false]]) ?>
  <div class="form-group has-feedback">
    <?= $this->Form->input('login', ['type' => 'text', 'class' => 'form-control', 'placeholder' => 'Login']) ?>
    <span class="glyphicon glyphicon-user form-control-feedback"></span>
  </div>
  <div class="form-group has-feedback">
    <?= $this->Form->input('senha', ['type' => 'password', 'class' => 'form-control', 'placeholder' => 'Senha']) ?>
    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
  </div>
  <div class="row">
    <div class="col-xs-12">
      <?= $this->Form->button('Entrar', ['class' => 'btn btn-primary btn-block btn-flat']) ?>
    </div><!-- /.col -->
  </div>
<?= $this->Form->end() ?>