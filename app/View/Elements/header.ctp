<header>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <img src="<?= $this->webroot ?>img/site/logo.png"/>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="bem-vindo">Seja muito bem vindo(a), <strong><?= $cadastro['Cadastro']['nome'] ?></strong></div>
            </div>
            <div class="col-lg-3 col-md-12 text-right">
                <?= $this->Html->link('Sair', ['action' => 'logout'], ['class' => 'sair']); ?>
            </div>
        </div>
    </div>
</header>