<?php
App::uses('AppController', 'Controller');

class UsuariosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = ['Paginator', 'Flash', 'Session'];

	public function beforeFilter() {
		parent::beforeFilter();
		if ($this->isCMS()) {
			$this->set('module_config', [
				'title'       => 'Usuários do CMS',
				'description' => ''
			]);
		}
	}

	public function login() {
        if ($this->request->is(['post','put'])) {
            if ($this->Auth->login()) {
            	$this->_setSidebarCollapse();
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Session->setFlash($this->Auth->authError, 'cms_msg', ['class' => 'danger', 'margin' => false], 'cms');
                return $this->redirect($this->Auth->loginAction);
            }
        }
        $this->layout = 'cms_login';
        $this->set('title_for_layout', 'Login | CMS');
    }

    public function logout() {
        $this->Session->setFlash(__('Deslogado com sucesso.'), 'cms_msg', ['class' => 'success', 'margin' => false], 'cms');
        $this->redirect($this->Auth->logout());
    }

    public function _setSidebarCollapse($collapse = 0) {
    	$this->Session->write('cms_sidebar_collapse', $collapse);
    }

    public function cms_sidebar() {
    	if (isset($this->request->query['collapse'])) {
    		$this->_setSidebarCollapse($this->request->query['collapse']);
    	}
    	$this->autoRender = false;
    }

/**
 * cms_index method
 *
 * @return void
 */
	public function cms_index() {
		$conditions = ['Usuario.id >' => 1];
		if (isset($this->request->query['busca']) && !empty($this->request->query['busca'])) {
			$conditions += ['OR' => [
				'Usuario.nome LIKE' => '%'.$this->request->query['busca'].'%',
				'Usuario.email LIKE' => '%'.$this->request->query['busca'].'%',
				'Usuario.login LIKE' => '%'.$this->request->query['busca'].'%'
			]];
			$this->request->data['Usuario'] = ['busca' => $this->request->query['busca']];
		}
		$this->Paginator->settings['conditions'] = $conditions;
		$this->Paginator->settings['order'] = ['Usuario.id' => 'asc'];
		$this->Usuario->recursive = 0;
		$this->set('the_list', $this->Paginator->paginate());
	}

/**
 * cms_add method
 *
 * @return void
 */
	public function cms_add() {
		$this->Usuario->create();
        if ($this->request->is(['post', 'put'])) {
            if ($this->Usuario->save($this->request->data))  {
                $this->Session->setFlash(__('Registro salvo com sucesso.'), 'cms_msg', ['class' => 'success'], 'cms');
                return $this->redirect(['cms' => true, 'action' => 'edit', $this->Usuario->id]);
            } else {
                $this->Session->setFlash(__('Ocorreu algum erro.'), 'cms_msg', ['class' => 'danger'], 'cms');
            }
        }
	}

/**
 * cms_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cms_edit($id = null) {
		if (!$this->Usuario->exists($id)) {
			throw new NotFoundException(__('Requisição inválida.'));
		}
		if ($this->request->is(['post', 'put'])) {
			$usuario = $this->Usuario->read(null, $id);
			$this->request->data['Usuario'] += $usuario['Usuario'];
			if ($this->Usuario->save($this->request->data)) {
				$this->Session->setFlash(__('Registro atualizado com sucesso.'), 'cms_msg', ['class' => 'success'], 'cms');
				return $this->redirect(['action' => 'index', 'cms' => true]);
			} else {
				$this->Session->setFlash(__('Ocorreu algum erro.'), 'cms_msg', ['class' => 'danger'], 'cms');
			}
		} else {
			$this->request->data = $this->Usuario->read(null, $id);
		}

		if (isset($this->request->data['Usuario']['senha'])) {
            $this->request->data['Usuario']['senha'] = '';            
        }
	}

/**
 * cms_alter method
 *
 * @param string $id
 * @return void
 */
	public function cms_alter($id = null) {
		$this->view = 'cms_alter';
        return $this->cms_edit($id);
	}

/**
 * cms_remove method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function cms_remove($id = null) {
		$this->Usuario->id = $id;
        if (!$this->Usuario->exists()) {
            throw new NotFoundException(__('Registro não encontrado.'));
        }
        if ($this->Usuario->delete()) {
            $this->Session->setFlash(__('Registro removido com sucesso.'), 'cms_msg', ['class' => 'success'], 'cms');
        } else {
            $this->Session->setFlash(__('Ocorreu algum erro.'), 'cms_msg', ['class' => 'danger'], 'cms');
        }
        return $this->redirect(['action' => 'index', 'cms' => true]);
	}
}