<?php
	if (!isset($active)) {
		$active = '';
	}
	$list = [
		'meus_dados_cadastrais' => 'Meus dados cadastrais',
		'financeiro'            => 'Financeiro',
		'faturas'               => 'Faturas',
		'solicitacoes'          => 'Solicitações'
	];
?>
<div class="col-lg-3 col-md-12 h100 border-right-dif">
	<div class="mt160"></div>
	<ul class="menu list-unstyled">
		<?php foreach ($list as $key => $label) : ?>
			<li><?= $this->Html->link($label, ['action' => $key], ['class' => ($key == $active ? 'ativo' : false)]) ?></li>
			<li><div class="mt20"></div></li>
		<?php endforeach; ?>
	</ul>
</div>