<?= $this->element('header') ?>

<div class="container-fluid">
    <div class="row">
        <?= $this->element('sidebar', ['active' => 'financeiro']) ?>

        <div class="col-lg-9 col-md-12 h100">
			<div class="mt160"></div>
            <div class="content-conteudo">
                <h1>Financeiro</h1>
                <div class="mt50"></div>
				<?php $count = count($boletos); ?>
				<?php foreach ($boletos as $k => $boleto) : ?>
					<?php if (!empty($boleto['Boleto']['arquivo'])) : ?>
						<a href="<?= $this->webroot ?>arquivos/boletos/<?= $boleto['Boleto']['arquivo'] ?>" target="_blank" class="item-download"><?= $boleto['Boleto']['titulo'] ?> <img src="<?= $this->webroot ?>img/site/down.png" /></a>
						<?php if ($k < $count - 1) : ?>
							<div class="mt30"></div>
						<?php endif; ?>
					<?php endif; ?>
				<?php endforeach; ?>
                <div class="mt50"></div>
				<?php if (!empty($boletos)) : ?>
					<?= $this->Html->link('baixar todos', ['action' => 'financeiro_zip'], ['class' => 'botao-padrao', 'target' => '_blank']) ?>
				<?php endif; ?>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        function h100() {
            $('.h100').css('height', window.innerHeight);
        }h100();

        $(window).load(h100).resize(h100);
    })
</script>