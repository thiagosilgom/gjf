<?php
/**
 * Usuario Model
 *
 */
class Cadastro extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'nome';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = [
		'empresa_id' => [
			'notBlank' => [
				'rule'    => 'notBlank',
				'message' => 'A empresa não pode ser vazia.'
			]
		],
		'nome' => [
			'notBlank' => [
				'rule'    => 'notBlank',
				'message' => 'O nome não pode ser vazio.'
			]
		],
		'email' => [
			'notBlank' => [
				'rule'    => 'notBlank',
				'message' => 'O e-mail não pode ser vazio.'
			],
			'email' => [
				'rule'    => 'email',
				'message' => 'O campo deve estar no formato de e-mail.'
			],
			'isUnique' => [
				'rule'    => 'isUnique',
				'message' => 'Este cadastro já existe.'
			]
		]
	];

	public $belongsTo = [
		'Empresa' => [
			'className'  => 'Empresa',
			'foreignKey' => 'empresa_id',
			'conditions' => '',
			'fields'     => ''
		]
	];

	public $hasMany = [
		'Solicitacao' => array(
			'className'  => 'Solicitacao',
			'foreignKey' => 'cadastro_id',
			'dependent'  => false,
			'conditions' => '',
			'fields'     => '',
			'order'      => ''
		),
		'Resposta' => array(
			'className'  => 'Resposta',
			'foreignKey' => 'cadastro_id',
			'dependent'  => false,
			'conditions' => '',
			'fields'     => '',
			'order'      => ''
		)
	];
}
