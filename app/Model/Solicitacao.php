<?php
/**
 * Usuario Model
 *
 */
class Solicitacao extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'titulo';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = [
		'empresa_id' => [
			'notBlank' => [
				'rule'    => 'notBlank',
				'message' => 'A empresa não pode ser vazia.'
			]
		],
		'cadastro_id' => [
			'notBlank' => [
				'rule'    => 'notBlank',
				'message' => 'O usuário da empresa não pode ser vazio.'
			]
		],
		'titulo' => [
			'notBlank' => [
				'rule'    => 'notBlank',
				'message' => 'O título não pode ser vazio.'
			]
		],
		'mensagem' => [
			'notBlank' => [
				'rule'    => 'notBlank',
				'message' => 'A mensagem não pode ser vazia.'
			]
		]
	];

	public $belongsTo = [
		'Empresa' => [
			'className'  => 'Empresa',
			'foreignKey' => 'empresa_id',
			'conditions' => '',
			'fields'     => ''
		],
		'Cadastro' => [
			'className'  => 'Cadastro',
			'foreignKey' => 'cadastro_id',
			'conditions' => '',
			'fields'     => ''
		]
	];

	public $hasMany = [
		'Resposta' => array(
			'className'  => 'Resposta',
			'foreignKey' => 'solicitacao_id',
			'dependent'  => true,
			'conditions' => '',
			'fields'     => '',
			'order'      => ['Resposta.id' => 'asc']
		)
	];
}
