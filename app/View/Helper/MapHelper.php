<?php
App::uses('AppHelper', 'View/Helper');

class MapHelper extends AppHelper {
	public function show($code, $width, $height) {
		if (empty($code)) {
			return '';
		}

		preg_match_all('/src\=\"([^\"]+)\"/', stripslashes($code), $matches);
		if (!isset($matches[1]) || empty($matches[1])) {
			return '';
		}
		
		return '<iframe src="' . $matches[1][0] . '" width="' . $width . '" height="' . $height . '" frameborder="0" style="border:0" allowfullscreen></iframe>';
	}
}