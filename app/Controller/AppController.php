<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('Util', 'Lib');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $helpers = ['Html', 'Form', 'Session', 'Media.Media', 'Image', 'Map'];
	public $components = [
		'Session',
		'Cookie',
        'Paginator',

        'Auth' => [
            'authenticate' => [
                'Form' => [
                    'userModel' => 'Usuario',
                    'fields'    => ['username' => 'login','password' => 'senha'],
                    'scope' => ['Usuario.ativo' => true],
                    'passwordHasher' => 'Blowfish',
                ],
            ],
            'authorize'      => 'Controller',
            'loginAction'    => ['controller' => 'usuarios', 'action' => 'login', 'plugin' => null, 'cms' => false],
            'logoutRedirect' => ['controller' => 'usuarios', 'action' => 'login', 'plugin' => null, 'cms' => false],
            'loginRedirect'  => '/cms',
            'authError'      =>  'Login e senha não conferem!',
        ],
	];

    public function canUploadMedias($model, $id) {
        if ($this->Auth->user()) {
            return true;
        }
        return false;
    }

    public function isCMS() {
        return (
            (isset($this->request->params['prefix']) && $this->request->params['prefix'] == 'cms') ||
            (isset($this->request->params['cms']) && $this->request->params['cms'])
        );
    }

	public function beforeFilter() {
        if ($this->isCMS()) {
            $this->layout = 'cms_default';
            $this->set('title_for_layout', 'CMS');
            $this->loadModel('Usuario');
            $usuario = $this->Usuario->read(null, $this->Auth->user('id'));
            $this->set('the_user', $usuario);
        } else {
            $this->Auth->allow();
            $this->loadModel('Config');
            $data = $this->Config->read(null, 1);
            $this->set('configs', $data['Config']);
            $this->set('title_for_layout', $data['Config']['titulo']);
        }
	}

    public function isAuthorized($user) {
        if (!empty($this->request->params['prefix']) && $this->request->params['prefix'] == 'cms') {
            return !empty($user);
        }  
        return true;
    }
}
