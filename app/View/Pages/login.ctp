<div class="container-fluid">
	<div class="row">
		<div class="col-lg-4 col-md-12 dt h100 h100-do-login">
			<div class="dt-c">
				<div class="login">
					<img src="<?= $this->webroot ?>img/site/logo.png" alt=""/>
					<div class="mt30"></div>
					<p><?= nl2br($configs['texto_login']) ?></p>
					<div class="mt30"></div>
					<?= $this->Session->flash('sistema') ?>
					<?= $this->Form->create('Login', ['inputDefaults' => ['div' => false, 'label' => false]]) ?>
						<?= $this->Form->input('email', ['type' => 'email', 'placeholder' => 'E-mail', 'required']) ?>
						<div class="mt20"></div>
						<?= $this->Form->input('senha', ['type' => 'password', 'placeholder' => 'Senha', 'required']) ?>
						<div class="mt20"></div>
						<?= $this->Form->input('entrar', ['type' => 'submit']) ?>
					<?= $this->Form->end() ?>
				</div>
			</div>
		</div>
		<div class="col-lg-8 col-md-12 h100 some-resp" style="background: url(<?= $this->webroot ?>img/site/login.png) no-repeat center center; background-size: cover !important;">
		</div>
	</div>
</div>

<script>
    $(document).ready(function(){
        function h100() {
            $('.h100').css('height', window.innerHeight);
        }h100();

        $(window).load(h100).resize(h100);
    })
</script>