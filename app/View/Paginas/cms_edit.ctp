<?php
	switch ($this->request->data('Pagina.id')) {
		case 1:
			$media_info = '';
		default:
			$media_info = '';
	}
?>

<?= $this->element('cms_module_header') ?>

<!-- Main content -->
<section class="content">

	<div class="box">
		<div class="box-header with-border">
			<i class="fa fa-edit"></i>
			<h3 class="box-title">Editar Página</h3>
		</div><!-- /.box-header -->

		<?= $this->Form->create('Pagina', ['inputDefaults' => ['div' => ['class' => 'col-xs-12 col-md-6 form-group'], 'class' => 'form-control']]) ?>
			<div class="box-body">
				<div class="row">
					<div class="col-xs-12">
						<?= $this->Html->link('<i class="fa fa-list"></i> Listagem', ['action' => 'index', 'cms' => true], ['escape' => false, 'class' => 'btn btn-app']) ?>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<?= $this->Session->flash('cms') ?>
					</div>
				</div>

				<div class="margin">
					<div class="row">
						<?= $this->Form->input('titulo', ['label' => 'Título']) ?>
					</div>
					<?php if ($this->request->data('Pagina.tem_texto')) : ?>
						<div class="row">
							<?php if ($this->request->data('Pagina.tem_editor')) : ?>
								<?= $this->Media->ckeditor('texto', ['label' => 'Texto', 'div' => 'col-xs-12 form-group']) ?>
							<?php else : ?>
								<?= $this->Form->input('texto', ['label' => 'Texto', 'div' => 'col-xs-12 form-group']) ?>
							<?php endif; ?>
						</div>
					<?php endif; ?>
				</div>

				<?php if ($this->request->data('Pagina.tem_midias')) : ?>
					<div class="margin">
						<div class="row">
							<?php if (!empty($media_info)) : ?>
								<div class="col-xs-12">
									<?= $this->element('cms_msg', ['message' => $media_info, 'close' => false]) ?>
								</div>
							<?php endif; ?>
							<div class="col-xs-12 midias-iframe">
								<?= $this->Media->iframe('Pagina', $this->request->data('Pagina.id')) ?>
							</div>
						</div>
					</div>
				<?php endif; ?>

				<?php if ($this->request->data('Pagina.tem_seo')) : ?>
					<div class="margin">
						<h4>SEO</h4>
						<div class="row">
							<?= $this->Form->input('seo_title', ['label' => 'Title', 'type' => 'text']) ?>
						</div>
						<div class="row">
							<?= $this->Form->input('seo_description', ['label' => 'Description', 'type' => 'text']) ?>
						</div>
					</div>
				<?php endif; ?>
			</div><!-- /.box-body -->

			<div class="box-footer">
				<?= $this->Form->input('id', ['type' => 'hidden']) ?>
				<?= $this->Form->button(__('Salvar'), ['class' => 'btn btn-lg btn-primary pull-right']) ?>
			</div>
		<?= $this->Form->end() ?>
	</div><!-- /.box -->

</section><!-- /.content -->