(function($){
    $.fn.pointerMaps = function () {
        var maps = this;

        $('iframe', maps).css("pointer-events", "none");

        maps.click(function () {
            $('iframe', maps).css("pointer-events", "auto");
        });

        maps.mouseleave(function () {
            $('iframe', maps).css("pointer-events", "none");
        });
    };
})(jQuery);