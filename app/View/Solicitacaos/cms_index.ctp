<?= $this->element('cms_module_header') ?>

<!-- Main content -->
<section class="content">

	<div class="box">
		<div class="box-header with-border">
			<i class="fa fa-list"></i>
			<h3 class="box-title">Listagem de Solicitações</h3>
		</div><!-- /.box-header -->
		<div class="box-body">
			<div class="row">
				<div class="col-xs-12">
					<?= $this->Html->link('<i class="fa fa-plus"></i> Adicionar', ['action' => 'add', 'cms' => true], ['escape' => false, 'class' => 'btn btn-app']) ?>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<?= $this->Form->create('Solicitacao', ['inputDefaults' => ['div' => false, 'label' => false], 'type' => 'get', 'url' => ['action' => 'index', 'cms' => true]]) ?>
						<div class="row" style="margin-right:10px">
							<div class="col-xs-12 form-group margin">
								<?= $this->Form->input('empresa_id', ['empty' => __('Empresa'), 'class' => 'form-control', 'required' => false]) ?>
							</div>
						</div>
						<div class="row" style="margin-right:10px">
							<div class="col-xs-12 form-group margin">
								<?= $this->Form->input('finalizado', ['empty' => __('Finalizado'), 'options' => [1 => 'Sim', 0 => 'Não'], 'class' => 'form-control']) ?>
							</div>
						</div>
						<div class="row" style="margin-right:10px">
							<div class="col-xs-12 form-group margin">
								<?= $this->Form->input('busca', ['placeholder' => __('ID, título, assunto ou mensagem...'), 'class' => 'form-control']) ?>
							</div>
						</div>
						<div class="row" style="margin-right:10px">
							<div class="col-xs-12 form-group margin">
								<?= $this->Form->button(__('Buscar'), ['class' => 'btn btn-info btn-flat pull-right']) ?>
							</div>
						</div>
					<?= $this->Form->end() ?>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<?= $this->Session->flash('cms') ?>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<?php if (empty($the_list)) : ?>
						<?= $this->element('cms_msg', ['message' => __('Nenhum registro encontrado.'), 'class' => 'warning', 'close' => false]) ?>
					<?php else : ?>
						<div class="margin">
							<div class="box-body table-responsive no-padding">
								<table class="table table-hover table-striped">
									<tbody>
										<tr>
											<th class="hidden-xs">ID</th>
											<th>Empresa</th>
											<th>Título</th>
											<th>Assunto</th>
											<th>Abertura</th>
											<th>Última Mensagem</th>
											<th class="hidden-sm hidden-md hidden-lg">Finalizado</th>
											<th class="hidden-xs">Ações</th>
										</tr>
										<?php foreach ($the_list as $item) : ?>
											<tr>
												<td class="hidden-xs"><?= $this->Html->link(Util::codigo($item['Solicitacao']['id']), ['action' => 'view', 'cms' => true, $item['Solicitacao']['id']], ['class' => 'text-black']) ?></td>
												<td><?= $this->Html->link($item['Empresa']['titulo'], ['action' => 'view', 'cms' => true, $item['Solicitacao']['id']], ['class' => 'text-black']) ?></td>
												<td><?= $this->Html->link($item['Solicitacao']['titulo'], ['action' => 'view', 'cms' => true, $item['Solicitacao']['id']], ['class' => 'text-black']) ?></td>
												<td><?= $this->Html->link($item['Solicitacao']['assunto'], ['action' => 'view', 'cms' => true, $item['Solicitacao']['id']], ['class' => 'text-black']) ?></td>
												<td><?= $this->Html->link((!empty($item['Solicitacao']['created']) ? date('d/m/Y H:i:s', strtotime($item['Solicitacao']['created'])) : ''), ['action' => 'view', 'cms' => true, $item['Solicitacao']['id']], ['class' => 'text-black']) ?></td>
												<td><?= $this->Html->link((!empty($item['Solicitacao']['data_ultima_mensagem']) ? date('d/m/Y H:i:s', strtotime($item['Solicitacao']['data_ultima_mensagem'])) : ''), ['action' => 'view', 'cms' => true, $item['Solicitacao']['id']], ['class' => 'text-black']) ?></td>
												<td class="hidden-sm hidden-md hidden-lg"><?= $this->Html->link($item['Solicitacao']['finalizado'] ? 'Sim' : 'Não', ['action' => 'view', 'cms' => true, $item['Solicitacao']['id']], ['class' => $item['Solicitacao']['finalizado'] ? 'text-green' : 'text-red']) ?></td>
												<td class="hidden-xs">
													<?=
														!$item['Solicitacao']['finalizado']
														? $this->Html->link('<i class="fa fa-question text-red"></i>', ['action' => 'finalizar', 'cms' => true, $item['Solicitacao']['id'], 1], ['escape' => false, 'class' => 'link-acao'])
														: $this->Html->link('<i class="fa fa-check text-green"></i>', ['action' => 'finalizar', 'cms' => true, $item['Solicitacao']['id'], 0], ['escape' => false, 'class' => 'link-acao'])
													?>
													<?= $this->Html->link('<i class="fa fa-wechat text-black"></i>', ['action' => 'view', 'cms' => true, $item['Solicitacao']['id']], ['escape' => false, 'class' => 'link-acao']) ?>
													<?= $this->Html->link('<i class="fa fa-remove text-black"></i>', ['action' => 'remove', 'cms' => true, $item['Solicitacao']['id']], ['escape' => false, 'class' => 'link-acao'], __('Deseja realmente remover o registro?')) ?>
												</td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>

			<?= $this->element('cms_paginacao') ?>
		</div><!-- /.box-body -->
	</div><!-- /.box -->

</section><!-- /.content -->