<?php
/**
 * Usuario Model
 *
 */
class Resposta extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = [
		'mensagem' => [
			'notBlank' => [
				'rule'    => 'notBlank',
				'message' => 'A mensagem não pode ser vazia.'
			]
		]
	];

	public $belongsTo = [
		'Solicitacao' => [
			'className'  => 'Solicitacao',
			'foreignKey' => 'solicitacao_id',
			'conditions' => '',
			'fields'     => ''
		],
		'Usuario' => [
			'className'  => 'Usuario',
			'foreignKey' => 'usuario_id',
			'conditions' => '',
			'fields'     => ''
		],
		'Cadastro' => [
			'className'  => 'Cadastro',
			'foreignKey' => 'cadastro_id',
			'conditions' => '',
			'fields'     => ''
		]
	];
}
