<?php
	header('Content-Type: application/csv; charset=utf-8');
	header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	header('Content-Length: ' . strlen($this->fetch('content')));
	header('Content-Disposition: attachment; filename=' . ($this->fetch('filename') ?: 'export') . '.csv');
	echo $this->fetch('content');